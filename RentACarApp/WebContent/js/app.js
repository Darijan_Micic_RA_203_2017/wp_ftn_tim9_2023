const RegistrationPage = { template: "<registration-form></registration-form>"}
const ProfilePage = { template: "<profile></profile>" }
const ViewRentACar = { template: "<rentACar-object></rentACar-object>" }
const ViewAllRentingObjects = { template: "<view-all-renting-objects></view-all-renting-objects>" }
const AdminViewUsers = { template: "<admin-view-users></admin-view-users>" }
const LoginPage = { template: "<login></login>" }
const CreateNewRentingObject = { template: "<create-new-renting-object></create-new-renting-object>" }
const ManagerViewObject = { template: "<manager-view-object></manager-view-object>" }
const ManagerViewComments = { template: "<manager-view-comments></manager-view-comments>" }
const CustomerRentings = { template: "<customer-view-rentings></customer-view-rentings>" }
const LeaveComment = { template: "<leaveComment></leaveComment>" }
const AddCar = { template: "<add-car></add-car>" }
const EditCar = { template: "<edit-car></edit-car>" }

const router = new VueRouter({
	mode: "hash",
	  routes: [
		{ path: "/", component: ViewAllRentingObjects },
		{ path: "/registration", component: RegistrationPage },
		{ path: "/profile", component: ProfilePage },
		{ path: "/rentACar/:id", component: ViewRentACar },
		{ path: "/view-all-renting-objects", component: ViewAllRentingObjects },
		{ path: "/allUsers", component: AdminViewUsers },
		{ path: "/login", component: LoginPage },
		{ path: "/create-new-renting-object", component: CreateNewRentingObject },
		{ path: "/managerObject", component: ManagerViewObject },
		{ path: "/managerComments", component: ManagerViewComments },
		{ path: "/customerRentings", component: CustomerRentings },
		{ path: "/leaveComment/:objectId/:customerId", component: LeaveComment },
		{ path: "/add-car", component: AddCar },
		{ path: "/editCar/:id", component: EditCar }
	  ]
});

var app = new Vue({
	router,
	el: "#app",
	methods: {
		GoToRegistrationPage: function() {
			this.$router.push("/registration");
		},
		GoToLoginPage: function() {
			this.$router.push("/login");
		},
		Logout: function() {
			axios.post("api/auth/logout").then(
				response => {
					console.log(response.data);
				},
				error => {
					alert(error.responseText);
				}
			);

			this.$router.push("/login");
		}
	}
});
