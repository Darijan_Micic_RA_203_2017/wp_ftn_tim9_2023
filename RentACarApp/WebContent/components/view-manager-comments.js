Vue.component("manager-view-comments", {
    data:function (){
        return{
         	object: {id: null, name: null, startHour: null, startMinute: null, endHour: null, endMinute: null, status: null, locationId: null, image: null, grade: null, cars: null},
         	pathId: null,
         	workingHours: null,
         	location: {id: null, longitude: null, latitude: null, address: null},
			cars: [],
			comments: null,
			commentUsers: [],
			loggedUser: null,
			rentings: null,
			rentingUsers: [],
			uniqueUsers: [],
			carsFromRenting: [],
			loggedUser: null
        }
    },
    template:
    `
       <div v-if="loggedUser.role === 'MANAGER'">
        	<div class="container d-flex flex-column">
	    	<h3 v-if="commentUsers.length !== 0">Komentari za objekat {{loggedUser.firstName}} {{loggedUser.lastName}}:</h3>
				<table class="table" v-if="commentUsers.length !== 0">
				<thead>
		    		<tr>
		    			<th>Korisnicko ime korisnika</th>
		    			<th>Komentar</th>
		    			<th>Ocena</th>
		    			<th>Status</th>
		    			<th></th>
		    			<th></th>
		    			<th></th>
		    			<th></th>
		    		</tr>
		    	</thead>
		    	<tbody>	
		    		<tr v-for="(c, index) in comments">
		    			<td>{{commentUsers[index].username}}</td>
		    			<td>{{c.comment}}</td>
		    			<td>{{c.rating}}</td>
		    			<td>{{c.status}}</td>
		    			<td v-if="c.status==='PENDING'"><button v-on:click="Prihvati(c.id)">Prihvati</button></td>
		    			<td v-if="c.status!=='PENDING'"></td>
		    			<td v-if="c.status==='PENDING'"><button v-on:click="Odbij(c.id)">Odbij</button></td>
		    			<td v-if="c.status!=='PENDING'"></td>
		    			<td><button v-on:click="Obrisi(c.id)">Obrisi</button></td>
		    			<td></td>
		    		</tr>
		    	</tbody>
	    	</table>
			</div>
        </div>
    `,
    mounted(){
			axios.get("api/auth/logged-user")
			.then(response =>{
				this.loggedUser = response.data
				
				axios.get("api/objects/" + this.loggedUser.rentACarObjectId).then(response=>
				{
					this.object = response.data
				}).then(response=> axios.get("api/comments/all/" + this.object.id).then(response=> this.comments = response.data))
					.then(response=> {
				for(let item of this.comments){
					axios.get("api/profiles/" + item.userId).then(response => this.commentUsers.push(response.data))

				}
			})
				
			})
			
			axios.get("api/auth/logged-user")
			.then(response =>	
				this.loggedUser = response.data).then(response=> {
					if(this.loggedUser.role!=="MANAGER"){
						window.alert("Nemate pravo pristupa!")
					}
				})
		
	}
		,
    methods: {
 		Prihvati : function(id){
			 const status = "ACCEPTED"
			 axios.put("api/comments/acceptOrReject/" + id, status)
			 this.$router.go()
		 },
		 Odbij : function(id){
			 const status = "REJECTED"
			 axios.put("api/comments/acceptOrReject/" + id, status)
			 this.$router.go()
		 },
		 Obrisi : function(id){
			 axios.put("api/comments/deleteComment/"+id).then(response=> console.log(response))
			 this.$router.go()
		 }
    }

})
