Vue.component("admin-view-users", {
    data:function (){
        return{
         	users: [{
				id: null,
        		logicallyDeleted: null,
        		username: null,
        		password: null,
        		firstName: null,
		        lastName: null,
		        gender: null,
		        dateOfBirth: null,
		        role: null,
		        collectedPoints: null,
		        customerTypeId: null
			 }],
         	sortedUsers: [],
         	sortOrder: 'ascending',
         	filteredRole: '',
         	filteredCustomerTypeId: '',
         	searchParams: {
		        firstName: '',
		        lastName: '',
		        username: ''
		      },
		      loggedUser: null
        }
    },
    template:
    `
        <div v-if="loggedUser.role === 'ADMIN'">
        	<div class="container d-flex flex-column">
			<h1>Registrovani korisnici:</h1>
			<div class="button-container">
				<button class="inline-button" @click="sortByName">Sortiraj po imenu</button>
				<button class="inline-button" @click="sortBySurname">Sortiraj po prezimenu</button>
				<button class="inline-button" @click="sortByUsername">Sortiraj po korisnickom imenu</button>
				<button class="inline-button" @click="sortByPoints">Sortiraj po poenima</button>
			</div>
			<div class="form-group">
				<br>
			    <label for="roleFilter">Filter by Role:</label>
			    <select id="roleFilter" v-model="filteredRole">
			        <option value="">All Roles</option>
			        <option value="ADMIN">Admin</option>
			        <option value="CUSTOMER">Customer</option>
			        <option value="MANAGER">Manager</option>
			    </select>
			    <label for="customerTypeFilter">Filter by Customer Type:</label>
			      <select id="customerTypeFilter" v-model="filteredCustomerTypeId">
			        <option value="">All Customer Types</option>
			        <option value="0">Nije kupac</option>
			        <option value="1">Zlatni</option>
			        <option value="2">Srebrni</option>
			        <option value="3">Bronzani</option>
			      </select>
			      <div class="form-group">
			      		<br>
			          <label for="firstNameInput">Ime:</label>
			          <input class="search-input" type="text" id="firstNameInput" v-model="searchParams.firstName">
			
			          <label for="lastNameInput">Prezime:</label>
			          <input class="search-input" type="text" id="lastNameInput" v-model="searchParams.lastName">
			
			          <label for="usernameInput">Korisnicko ime:</label>
			          <input class="search-input" type="text" id="usernameInput" v-model="searchParams.username">
			
			          <button class="inline-button" @click="searchUsers">Pretraga</button>
			        </div>
			        <br>
				<table class="table">
				<thead>
		    		<tr>
		    			<th>Korisnicko ime</th>
		    			<th>Ime</th>
		    			<th>Prezime</th>
		    			<th>Pol</th>
		    			<th>Datum rodjenja</th>
		    			<th>Uloga</th>
		    			<th>Sakupljeni poeni</th>
		    			<th>Tip kupca</th>
		    		</tr>
		    	</thead>
		    	<tbody>	
		    		<tr v-for="(user, index) in filteredUsers">
		    			<td v-if="!user.logicallyDeleted">{{user.username}}</td>
		    			<td v-if="!user.logicallyDeleted">{{user.firstName}}</td>
		    			<td v-if="!user.logicallyDeleted">{{user.lastName}}</td>
		    			<td v-if="!user.logicallyDeleted">{{user.gender}}</td>
		    			<td v-if="!user.logicallyDeleted">{{user.dateOfBirth}}</td>
		    			<td v-if="!user.logicallyDeleted">{{user.role}}</td>
		    			<td v-if="!user.logicallyDeleted">{{user.collectedPoints}}</td>
		    			<td v-if="user.customerTypeId===0 && !user.logicallyDeleted">Nije kupac</td>
		    			<td v-if="user.customerTypeId===1 && !user.logicallyDeleted">Zlatni</td>
		    			<td v-if="user.customerTypeId===2 && !user.logicallyDeleted">Srebrni</td>
		    			<td v-if="user.customerTypeId===3 && !user.logicallyDeleted">Bronzani</td>
		    		</tr>
		    	</tbody>
	    	</table>
			</div>
	</div>
        </div>
    `,
    mounted(){
		axios.get("api/allUsers")
    .then(response => {
		this.users = response.data
		console.log(this.users)
		this.sortedUsers = response.data
		 })
		 
		 axios.get("api/auth/logged-user")
			.then(response =>	
				this.loggedUser = response.data).then(response=> {
					if(this.loggedUser.role!=="ADMIN"){
						window.alert("Nemate pravo pristupa!")
					}
				})
		
}	
		,
computed: {
  		filteredUsers() {
		    let filteredUsers = this.sortedUsers; // Start with all sorted users

		    if (this.filteredRole) {
		      filteredUsers = filteredUsers.filter(user => user.role === this.filteredRole);
		    }
		
		    if (this.filteredCustomerTypeId) {
		      filteredUsers = filteredUsers.filter(user => user.customerTypeId == this.filteredCustomerTypeId);
		    }
		
		    return filteredUsers;
		  }
},
    methods: {
 		sortByName() {
			this.sortOrder = this.sortOrder === 'ascending' ? 'descending' : 'ascending'
    		this.sortedUsers.sort((a, b) => {
      		const nameA = a.firstName.toUpperCase();
      		const nameB = b.firstName.toUpperCase();
		      if (nameA < nameB) {
		         return this.sortOrder === 'ascending' ? -1 : 1;
		      }
		      if (nameA > nameB) {
		       return this.sortOrder === 'ascending' ? 1 : -1;
		      }
		      return 0;
		    })},
		sortBySurname(){
			this.sortOrder = this.sortOrder === 'ascending' ? 'descending' : 'ascending'
    		this.sortedUsers.sort((a, b) => {
      		const nameA = a.lastName.toUpperCase();
      		const nameB = b.lastName.toUpperCase();
		      if (nameA < nameB) {
		         return this.sortOrder === 'ascending' ? -1 : 1;
		      }
		      if (nameA > nameB) {
		        return this.sortOrder === 'ascending' ? 1 : -1;
		      }
		      return 0;
		    })},
		sortByUsername() {
			this.sortOrder = this.sortOrder === 'ascending' ? 'descending' : 'ascending'
    		this.sortedUsers.sort((a, b) => {
      		const nameA = a.username.toUpperCase();
      		const nameB = b.username.toUpperCase();
		      if (nameA < nameB) {
		         return this.sortOrder === 'ascending' ? -1 : 1;
		      }
		      if (nameA > nameB) {
		        return this.sortOrder === 'ascending' ? 1 : -1;
		      }
		      return 0;
		    })},
		 sortByPoints() {
			this.sortOrder = this.sortOrder === 'ascending' ? 'descending' : 'ascending'
    		this.sortedUsers.sort((a, b) => {
      		const nameA = a.collectedPoints;
      		const nameB = b.collectedPoints;
		      if (nameA < nameB) {
		         return this.sortOrder === 'ascending' ? -1 : 1;
		      }
		      if (nameA > nameB) {
		       return this.sortOrder === 'ascending' ? 1 : -1;
		      }
		      return 0;
		    })},
		    searchUsers() {
		      axios.get("api/allUsers/search", { params: this.searchParams })
		        .then(response => {
		          this.sortedUsers = response.data;
        });
    }
		    
		  }

})
