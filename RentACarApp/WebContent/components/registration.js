Vue.component("registration-form", {
	data: function() {
		return {
			newCustomer: {
				id: 0,
				logicallyDeleted: false,
				username: null,
				password: null,
				firstName: null,
				lastName: null,
				gender: null,
				dateOfBirth: null,
				role: "CUSTOMER",
				collectedPoints: 0,
				customerTypeId: 3,
				rentACarObjectId: 0
			},
			passwordConfirmation: null,
			usernameErrorMessage: null,
			passwordErrorMessage: null,
			passwordConfirmationErrorMessage: null,
			firstNameErrorMessage: null,
			lastNameErrorMessage: null,
			genderErrorMessage: null,
			dateOfBirthErrorMessage: null
		}
	},
	template: `
	<div class="container d-flex flex-column">
		<h1>Registracija kupca</h1>
		<form role="form" id="registrationForm">
			<div class="form-group">
				<label for="usernameInput">Korisničko ime:</label>
				<input type="text" class="form-control" id="usernameInput" 
						v-model="newCustomer.username">
				<p v-if="usernameErrorMessage != null">{{ usernameErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label for="passwordInput">Lozinka:</label>
				<input type="password" class="form-control" id="passwordInput" 
						v-model="newCustomer.password">
				<p v-if="passwordErrorMessage != null">{{ passwordErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label for="passwordConfirmationInput">Ponovo unesite lozinku:</label>
				<input type="password" class="form-control" id="passwordConfirmationInput" 
						v-model="passwordConfirmation">
				<p v-if="passwordConfirmationErrorMessage != null">{{ passwordConfirmationErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label for="firstNameInput">Ime:</label>
				<input type="text" class="form-control" id="firstNameInput" 
						v-model="newCustomer.firstName">
				<p v-if="firstNameErrorMessage != null">{{ firstNameErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label for="lastNameInput">Prezime:</label>
				<input type="text" class="form-control" id="lastNameInput" 
						v-model="newCustomer.lastName">
				<p v-if="lastNameErrorMessage != null">{{ lastNameErrorMessage }}</p>
			</div>
			<div class="form-group">
				<div class="form-check form-check-inline">
					<input type="radio" class="form-check-input" 
							name="genderInputRadioGroup" id="maleGenderInput" 
							value="MALE" 
							v-model="newCustomer.gender">
					<label class="form-check-label" for="maleGenderInput">Muški pol</label>
				</div>
				<div class="form-check form-check-inline">
					<input type="radio" class="form-check-input" 
							name="genderInputRadioGroup" id="femaleGenderInput" 
							value="FEMALE" 
							v-model="newCustomer.gender">
					<label class="form-check-label" for="femaleGenderInput">Ženski pol</label>
				</div>
				<p v-if="genderErrorMessage != null">{{ genderErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label for="dateOfBirthInput">Datum rođenja:</label>
				<input type="date" class="form-control" id="dateOfBirthInput" 
						v-model="newCustomer.dateOfBirth">
				<p v-if="dateOfBirthErrorMessage != null">{{ dateOfBirthErrorMessage }}</p>
			</div>
			<div class="submitButtonDiv">
				<button type="submit" class="btn-default bg-primary text-white" 
						v-on:click="RegisterNewCustomer($event)">Registrujte se</button>
			</div>
		</form>
	</div>
	`,
	mounted() {},
	methods: {
		RegisterNewCustomer: function(event) {
			event.preventDefault();
			
			if (this.IsRegistrationFormValid()) {
				axios.post("api/allUsers/registerCustomer", this.newCustomer).then(
					response => {
						console.log(response.data);
						router.push(`/login`)
					}
					
				).catch(error => {
        			if (error.response && error.response.status === 400) {
          				console.error("Bad Request: ", error.response.data.message);
          				window.alert("Korisnicko ime je zauzeto!");
        			}
			 })
			}
		},
		IsRegistrationFormValid: function() {
			let formValidity = true;
			
			if (!this.IsUsernameValid()) {
				this.usernameErrorMessage = "Korisničko ime mora početi slovom, ne " + 
						"sme sadržati razmake i specijalne znakove (osim povlake, " + 
						"donje crte i tačke)!";
				
				formValidity = false;
			} else {
				this.usernameErrorMessage = null;
			}
			
			if (!this.IsPasswordValid()) {
				this.passwordErrorMessage = "Lozinka mora sadržati između 5 i 15 znakova!";
				
				formValidity = false;
			} else {
				this.passwordErrorMessage = null;
			}
			
			if (!this.DoPasswordsMatch()) {
				this.passwordConfirmationErrorMessage = "Lozinka mora biti ponovo " + 
						"uneta!";
				
				formValidity = false;
			} else {
				this.passwordConfirmationErrorMessage = null;
			}
			
			if (!this.IsFirstNameValid()) {
				this.firstNameErrorMessage = "Ime mora započeti velikim slovom i ne " + 
						"sme sadržati brojeve!";
				
				formValidity = false;
			} else {
				this.firstNameErrorMessage = null;
			}
			
			if (!this.IsLastNameValid()) {
				this.lastNameErrorMessage = "Prezime mora započeti velikim slovom i " + 
						"ne sme sadržati brojeve!";
				
				formValidity = false;
			} else {
				this.lastNameErrorMessage = null;
			}
			
			if (!this.IsGenderValid()) {
				this.genderErrorMessage = "Pol mora biti odabran!";
				
				formValidity = false;
			} else {
				this.genderErrorMessage = null;
			}
			
			if (!this.IsDateOfBirthValid()) {
				this.dateOfBirthErrorMessage = "Datum rođenja mora biti unet!";
				
				formValidity = false;
			} else {
				this.dateOfBirthErrorMessage = null;
			}
			
			return formValidity;
		},
		IsUsernameValid: function() {
			let username = this.newCustomer.username;
			
			if (typeof username !== "string") {
				return false;
			}
			
			let usernameRegexp = /^[a-zA-Z][-_a-zA-Z0-9\.]+$/;
			if (usernameRegexp.test(username)) {
				return true;
			}
			
			return false;
		},
		IsPasswordValid: function() {
			let password = this.newCustomer.password;
			
			if (password != null) {
				if (typeof password === "string") {
					if (password.length >= 5 && password.length <= 15) {
						return true;
					}
				}
			}
			
			return false;
		},
		DoPasswordsMatch: function() {
			let passwordConfirmation = this.passwordConfirmation;
			
			if (passwordConfirmation != null) {
				if (typeof passwordConfirmation === "string") {
					if (passwordConfirmation === this.newCustomer.password) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsFirstNameValid: function() {
			let firstName = this.newCustomer.firstName;
			
			if (typeof firstName !== "string") {
				return false;
			}
			
			let nameRegexp = /^[A-Z\p{L}][a-z\p{L}]+([ -][A-Z\p{L}][a-z\p{L}]+)*$/u;
			if (nameRegexp.test(firstName)) {
				return true;
			}
			
			return false;
		},
		IsLastNameValid: function() {
			let lastName = this.newCustomer.lastName;
			
			if (typeof lastName !== "string") {
				return false;
			}
			
			let nameRegexp = /^[A-Z\p{L}][a-z\p{L}]+([ -][A-Z\p{L}][a-z\p{L}]+)*$/u;
			if (nameRegexp.test(lastName)) {
				return true;
			}
			
			return false;
		},
		IsGenderValid: function() {
			let gender = this.newCustomer.gender;
			
			if (gender != null) {
				if (typeof gender === "string") {
					if (gender === "MALE" || gender === "FEMALE") {
						return true;
					}
				}
			}
			
			return false;
		},
		IsDateOfBirthValid: function() {
			let dateOfBirth = this.newCustomer.dateOfBirth;
			
			if (typeof dateOfBirth !== "string") {
				return false;
			}
			
			let dateOfBirthRegexp = /^(19|20)[0-9]{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/;
			if (dateOfBirthRegexp.test(dateOfBirth)) {
				return true;
			}
			
			return false;
		}
	}
});
