Vue.component("manager-view-object", {
    data:function (){
        return{
         	object: {id: null, name: null, startHour: null, startMinute: null, endHour: null, endMinute: null, status: null, locationId: null, image: null, grade: null, cars: null},
         	pathId: null,
         	workingHours: null,
         	location: {id: null, longitude: null, latitude: null, address: null},
			cars: [],
			comments: null,
			commentUsers: [],
			loggedUser: null,
			rentings: null,
			rentingUsers: [],
			uniqueUsers: [],
			carsFromRenting: [],
			loggedUser: null
        }
    },
    template:
    `
       <div v-if="loggedUser.role === 'MANAGER'">
        	<div class="container d-flex flex-column">
		<h1>Rent A Car Objekat od {{loggedUser.firstName}} {{loggedUser.lastName}}: <img :src="object.image"></h1>
			<div class="form-group">
				<label>Ime:</label>
				<input type="text" class="form-control" v-model="object.name" disabled>
			</div>
			<div class="form-group">
				<label>Radno vreme:</label>
				<input type="text" class="form-control" v-model="workingHours" disabled>
			</div>
			<div class="form-group">
				<label>Status:</label>
				<input type="text" class="form-control" v-model="object.status" disabled>
			</div>
			<div class="form-group">
				<label v-if="object.grade != 0">Ocena:</label>
				<input v-if="object.grade != 0" type="text" class="form-control" v-model="object.grade" disabled>
			</div>
			<div class="form-group">
				<label>Adresa:</label>
				<input type="text" class="form-control" v-model="location.address" disabled>
			</div>
			<div class="form-group">
				<label>Longituda:</label>
				<input type="text" class="form-control" v-model="location.longitude" disabled>
			</div>
			<div class="form-group">
				<label>Latituda:</label>
				<input type="text" class="form-control" v-model="location.latitude" disabled>
			</div>
			<button type="button" class="btn bg-primary text-white" v-on:click="AddNewCarToRentingObject">
				Dodajte novi automobil
			</button>
			<div class="form-group">
			<br>
				<h3>Kola:</h3>
				<table class="table">
				<thead>
		    		<tr>
		    			<th>Brend</th>
		    			<th>Cena</th>
		    			<th>Tip</th>
		    			<th>Vrsta</th>
		    			<th>Gorivo</th>
		    			<th>Potrosnja</th>
		    			<th>Broj vrata</th>
		    			<th>Max putnika</th>
		    			<th>Opis</th>
		    			<th>Slika</th>
		    			<th>Status</th>
		    			<th>O</th>
		    			<th>I</th>
		    		</tr>
		    	</thead>
		    	<tbody>	
		    		<tr v-for="car in cars">
		    			<td v-if="!car.logicallyDeleted">{{car.brand}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.price}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.type}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.kind}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.fuel}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.consumption}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.noDoors}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.noPeople}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.description}}</td>
		    			<td v-if="!car.logicallyDeleted"><img :src="car.image"></td>
		    			<td v-if="!car.logicallyDeleted">{{car.status}}</td>
		    			<td v-if="!car.logicallyDeleted"><button v-on:click="Obrisi(car.id)">Obrisi</button></td>
		    			<td v-if="!car.logicallyDeleted"><button v-on:click="Izmeni(car.id)">Izmeni</button></td>  
		    		</tr>
		    	</tbody>
	    	</table>
	    	<br>
	    	<h3 v-if="commentUsers.length !== 0">Komentari:</h3>
				<table class="table" v-if="commentUsers.length !== 0">
				<thead>
		    		<tr>
		    			<th>Korisnicko ime korisnika</th>
		    			<th>Komentar</th>
		    			<th>Ocena</th>
		    		</tr>
		    	</thead>
		    	<tbody>	
		    		<tr v-for="(c, index) in comments">
		    			<td>{{commentUsers[index].username}}</td>
		    			<td>{{c.comment}}</td>
		    			<td>{{c.rating}}</td>
		    		</tr>
		    	</tbody>
	    	</table>
			</div>
			<br>
	    	<h3>Narudzbine:</h3>
				<table class="table">
				<thead>
		    		<tr>
		    			<th>Sifra</th>
		    			<th>Kola</th>
		    			<th>Datum</th>
		    			<th>Trajanje</th>
		    			<th>Cena</th>
		    			<th>Kupac</th>
		    			<th>Status</th>
		    		</tr>
		    	</thead>
		    	<tbody>	
		    		<tr v-for="(c, index) in rentings">
		    			<td>{{c.id}}</td>
		    			<td>{{carsFromRenting[index][0].brand}}<label v-if="carsFromRenting[index][1]!==undefined"> ;{{carsFromRenting[index][1].brand}}</label></td>
		    			<td>{{c.date}}</td>
		    			<td>{{c.length}}</td>
		    			<td>{{c.price}}</td>
		    			<td>{{rentingUsers[index].firstName}} {{rentingUsers[index].lastName}}</td>
		    			<td>{{c.status}}</td>
		    		</tr>
		    	</tbody>
	    	</table>
	    	<br>
	    	<h3>Svi kupci iz objekta:</h3>
	    	<table class="table">
				<thead>
		    		<tr>
		    			<th>Korisnicko ime</th>
		    			<th>Ime</th>
		    			<th>Prezime</th>
		    			<th>Pol</th>
		    			<th>Datum rodjenja</th>
		    			<th>Uloga</th>
		    			<th>Sakupljeni poeni</th>
		    			<th>Tip kupca</th>
		    		</tr>
		    	</thead>
		    	<tbody>	
		    		<tr v-for="(user, index) in uniqueUsers">
		    			<td>{{user.username}}</td>
		    			<td>{{user.firstName}}</td>
		    			<td>{{user.lastName}}</td>
		    			<td>{{user.gender}}</td>
		    			<td>{{user.dateOfBirth}}</td>
		    			<td>{{user.role}}</td>
		    			<td>{{user.collectedPoints}}</td>
		    			<td v-if="user.customerTypeId===0">Nije kupac</td>
		    			<td v-if="user.customerTypeId===1">Zlatni</td>
		    			<td v-if="user.customerTypeId===2">Srebrni</td>
		    			<td v-if="user.customerTypeId===3">Bronzani</td>
		    		</tr>
		    	</tbody>
	    	</table>
			</div>
			
	</div>
        </div>
    `,
    mounted(){
			axios.get("api/auth/logged-user")
			.then(response =>{
				this.loggedUser = response.data
				
				axios.get("api/rentings/" + this.loggedUser.rentACarObjectId).then(response=>
				{
					this.rentings = response.data
					const ids = []
					for (let i = 0; i < this.rentings.length; i++) {
        				const renting = this.rentings[i];
        				ids.push(renting.customerId)
        				axios.get("api/allUsers/getCustomer/" + renting.customerId).then(response=>
        				{
							this.rentingUsers.push(response.data)
							
						})
        			}
        			const uniqueIds = [...new Set(ids)]
        			for (let i = 0; i < uniqueIds.length; i++) {
        				const id = uniqueIds[i];
        				axios.get("api/allUsers/getCustomer/" + id).then(response=>
        				{
							this.uniqueUsers.push(response.data)
							
						})
        			}
        			const carIds = []
        			for (let i = 0; i < this.rentings.length; i++) {
						
        				const renting = this.rentings[i];
        				const carIdsRenting = []
        				for(let i = 0; i < renting.carsIds.length; i++){
							
							const carId = renting.carsIds[i]
							axios.get("api/cars/" + carId).then(response=>carIdsRenting.push(response.data))
						}
        				carIds.push(carIdsRenting)
        			}
        			this.carsFromRenting = carIds;  
				})
				
				axios.get("api/objects/" + this.loggedUser.rentACarObjectId).then(response=>
				{
					this.object = response.data
					this.workingHours = this.object.startHour.toString() + ":" + this.object.startMinute.toString()
			 			+ " - " + this.object.endHour.toString() + ":" + this.object.endMinute.toString()
			 		axios.get("api/locations/" + this.object.locationId).then(response=> 
			 		{
						 this.location = response.data
					 })
					for(let item of this.object.cars){
					axios.get("api/cars/" + item).then(response=>this.cars.push(response.data))
					}
				}).then(response=> axios.get("api/comments/acceptedAndRejectedComments/" + this.object.id).then(response=> this.comments = response.data))
					.then(response=> {
				for(let item of this.comments){
					axios.get("api/profiles/" + item.userId).then(response => this.commentUsers.push(response.data))

				}
			})
				
			})
			
			axios.get("api/auth/logged-user")
			.then(response =>	
				this.loggedUser = response.data).then(response=> {
					if(this.loggedUser.role!=="MANAGER"){
						window.alert("Nemate pravo pristupa!")
					}
				})
		
	}
		,
    methods: {
		AddNewCarToRentingObject: function() {
			this.$router.push("/add-car");
		},
 		Obrisi : function(id){
			 
			 axios.put("api/cars/deleteCar/" + id).then(response=>console.log(response.data))
			 this.$router.go()
		 },
		 Izmeni : function(id){
			 
			 this.$router.push(`/editCar/${id}`)
		 }
    }

})
