Vue.component("edit-car", {
    data:function (){
        return{
            id: null, 
            car: null
        }
    },
    template:
    `
        <div>
        	<div class="container d-flex flex-column">
		<h1>Kola:</h1>
		<form role="form" id="profileForm">
			<div class="form-group">
				<label>Brend:</label>
				<input type="text" class="form-control" v-model="car.brand">
			</div>
			<div class="form-group">
				<label>Cena:</label>
				<input type="text" class="form-control" v-model="car.price">
			</div>
			<div class="form-group">
				<label>Tip:</label>
				 <select class="form-control" v-model="car.type">
				 	<option value=""></option>
			        <option value="AUTO">AUTO</option>
			        <option value="COMBI">COMBI</option>
			        <option value="MOBILEHOME">MOBILEHOME</option>
			    </select>
			</div>
			<div class="form-group">
				<label>Vrsta:</label>
				<select class="form-control" v-model="car.kind">
					<option value=""></option>
			        <option value="MANUEL">MANUEL</option>
			        <option value="AUTOMATIC">AUTOMATIC</option>
			    </select>
			</div>
			<div class="form-group">
				<label>Tip goriva:</label>
				<select class="form-control" v-model="car.fuel">
					<option value=""></option>
			        <option value="DISEL">DISEL</option>
			        <option value="BENZIN">BENZIN</option>
			        <option value="HYBRID">HYBRID</option>
			        <option value="ELECTRIC">ELECTRIC</option>
			    </select>
			</div>
			<div class="form-group">
				<label>Potrosnja:</label>
				<input type="number" class="form-control" v-model="car.consumption">
			</div>
			<div class="form-group">
				<label>Broj vrata:</label>
				<input type="number" class="form-control" v-model="car.noDoors">
			</div>
			<div class="form-group">
				<label>Broj mesta:</label>
				<input type="number" class="form-control" v-model="car.noPeople">
			</div>
			<div class="form-group">
				<label>Opis:</label>
				<input type="text" class="form-control" v-model="car.description">
			</div>
			<div class="form-group">
				<label>Slika:</label>
				<img :src="car.image" class="form-control">
				<select class="form-control" v-model="car.image">
					<option value="images/Mercedes_E_klasa.jpeg">Mercedes E</option>
			        <option value="images/car2.jpg">Crvena kola</option>
			        <option value="images/car1.jpg">Plava kola</option>
			    </select>
			</div>
			<div class="form-group">
				<label>Status:</label>
				<select class="form-control" v-model="car.status">
					<option value=""></option>
			        <option value="FREE">FREE</option>
			        <option value="BOOKED">BOOKED</option>
			    </select>
			</div>
			<div class="submitButtonDiv">
				<button type="submit" class="btn-default bg-primary text-white" v-on:click="Izmeni()">Izmeni</button>
			</div>
		</form>
	</div>
        </div>
    `,
    mounted(){
		this.id = this.$route.params.id;
		axios.get("api/cars/" + this.id).then(response=> this.car = response.data)
		
    },
    methods: {
		Izmeni:function(){
			event.preventDefault()
			console.log(this.car)
			axios.put("api/cars/editCar", this.car).then(response=>router.push(`/managerObject`))
		}
		
    }

})
