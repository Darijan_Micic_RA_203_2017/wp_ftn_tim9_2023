Vue.component("create-new-renting-object", {
	data: function() {
		return {
            loggedUser: null,
			allManagersWhoDoNotHaveARentingObject: [],
            newRentingObject: {
                id: 0,
                logicallyDeleted: false,
                name: null,
                startHour: null,
                startMinute: null,
                endHour: null,
                endMinute: null,
                status: "NOTWORKING",
                locationId: 0,
                image: null,
                grade: 0.0,
                cars: []
            },
            locationOfNewRentingObject: {
                id: 0,
				longitude: null,
				latitude: null,
				address: null
            },
			nameOfNewRentingObjectErrorMessage: null,
			locationLongitudeOfNewRentingObjectErrorMessage: null,
			locationLatitudeOfNewRentingObjectErrorMessage: null,
			locationAddressOfNewRentingObjectErrorMessage: null,
			startHourOfNewRentingObjectErrorMessage: null,
			startMinuteOfNewRentingObjectErrorMessage: null,
			endHourOfNewRentingObjectErrorMessage: null,
			endMinuteOfNewRentingObjectErrorMessage: null,
			logoOfNewRentingObjectErrorMessage: null,
			managerOfNewRentingObjectErrorMessage: null,
			newManagerIsNeeded: false,
			managerOfNewRentingObject: {
				id: 0,
				logicallyDeleted: false,
				username: null,
				password: null,
				firstName: null,
				lastName: null,
				gender: null,
				dateOfBirth: null,
				role: "MANAGER",
				collectedPoints: 0,
				customerTypeId: 0,
				rentACarObjectId: 0
			},
			passwordConfirmation: null,
			usernameErrorMessage: null,
			passwordErrorMessage: null,
			passwordConfirmationErrorMessage: null,
			firstNameErrorMessage: null,
			lastNameErrorMessage: null,
			genderErrorMessage: null,
			dateOfBirthErrorMessage: null
		}
	},
	template: `
    <div v-if="loggedUser.role == 'ADMIN'">
	<div class="container d-flex flex-column">
        <h1>Novi objekat za iznajmljivanje</h1>
        <form role="form" id="formForCreatingANewRentingObject">
            <div class="form-group">
                <label for="newRentingObjectNameInput">Naziv:</label>
                <input type="text" class="form-control" id="newRentingObjectNameInput" 
                        v-model="newRentingObject.name">
                <p v-if="nameOfNewRentingObjectErrorMessage != null">
					{{ nameOfNewRentingObjectErrorMessage }}
				</p>
            </div>
            <div class="form-group">
                <label for="locationLongitudeOfNewRentingObjectInput">Geografska dužina:</label>
                <input type="number" class="form-control" id="locationLongitudeOfNewRentingObjectInput" 
                        v-model="locationOfNewRentingObject.longitude">
                <p v-if="locationLongitudeOfNewRentingObjectErrorMessage != null">
                    {{ locationLongitudeOfNewRentingObjectErrorMessage }}
                </p>
            </div>
            <div class="form-group">
                <label for="locationLatitudeOfNewRentingObjectInput">Geografska širina:</label>
                <input type="number" class="form-control" id="locationLatitudeOfNewRentingObjectInput" 
                        v-model="locationOfNewRentingObject.latitude">
                <p v-if="locationLatitudeOfNewRentingObjectErrorMessage != null">
                    {{ locationLatitudeOfNewRentingObjectErrorMessage }}
                </p>
            </div>
            <div class="form-group">
                <label for="locationAddressOfNewRentingObjectInput">Adresa:</label>
                <input type="text" class="form-control" id="locationAddressOfNewRentingObjectInput" 
                        v-model="locationOfNewRentingObject.address">
                <p v-if="locationAddressOfNewRentingObjectErrorMessage != null">
                    {{ locationAddressOfNewRentingObjectErrorMessage }}
                </p>
            </div>
            <div class="form-group">
                <label for="startHourOfNewRentingObjectInput">Pocetak radnog vremena (čas i vreme):</label>
                <input type="number" class="form-control" id="startHourOfNewRentingObjectInput" 
                        v-model="newRentingObject.startHour">
                <p v-if="startHourOfNewRentingObjectErrorMessage != null">
					{{ startHourOfNewRentingObjectErrorMessage }}
				</p>
				<input type="number" class="form-control" id="startMinuteOfNewRentingObjectInput" 
                        v-model="newRentingObject.startMinute">
                <p v-if="startMinuteOfNewRentingObjectErrorMessage != null">
					{{ startMinuteOfNewRentingObjectErrorMessage }}
				</p>
            </div>
            <div class="form-group">
                <label for="endHourOfNewRentingObjectInput">Kraj radnog vremena (čas i vreme):</label>
                <input type="number" class="form-control" id="endHourOfNewRentingObjectInput" 
                        v-model="newRentingObject.endHour">
                <p v-if="endHourOfNewRentingObjectErrorMessage != null">
					{{ endHourOfNewRentingObjectErrorMessage }}
				</p>
				<input type="number" class="form-control" id="endMinuteOfNewRentingObjectInput" 
                        v-model="newRentingObject.endMinute">
                <p v-if="endMinuteOfNewRentingObjectErrorMessage != null">
					{{ endMinuteOfNewRentingObjectErrorMessage }}
				</p>
            </div>
			<div class="form-group">
                <label for="logoOfNewRentingObjectSelect">Logo:</label>
				<select class="form-control" id="logoOfNewRentingObjectSelect" 
                        v-model="newRentingObject.image">
					<option value="images/Objekat_zuti_ikona.png">Objekat_zuti_ikona.png</option>
					<option value="images/Objekat_narandzasti.png">Objekat_narandzasti.png</option>
				</select>
				<img v-bind:src="newRentingObject.image" class="form-control">
                <p v-if="logoOfNewRentingObjectErrorMessage != null">
					{{ logoOfNewRentingObjectErrorMessage }}
				</p>
            </div>
			<div class="form-group">
                <label for="managerOfNewRentingObjectSelect">Menadžer:</label>
				<select class="form-control" id="managerOfNewRentingObjectSelect" 
                        v-model="managerOfNewRentingObject">
					<option v-for="m of allManagersWhoDoNotHaveARentingObject" v-bind:value="m">
						{{ m.id + " " + m.firstName + " " + m.lastName }}
					</option>
				</select>
				<button type="button" class="btn bg-primary text-white" v-on:click="ShowFormForCreatingANewManager">
					Napravite novog menadžera
				</button>
				<p v-if="managerOfNewRentingObjectErrorMessage != null">
					{{ managerOfNewRentingObjectErrorMessage }}
				</p>
            </div>
            <div class="submitButtonDiv">
                <button type="submit" class="btn-default bg-primary text-white" 
						v-on:click="CreateNewRentingObject($event)">
                    Napravite novi objekat za iznajmljivanje vozila
                </button>
            </div>
        </form>
		<form v-if="newManagerIsNeeded" role="form" id="newManagerForm">
			<div class="form-group">
				<label for="usernameInput">Korisničko ime:</label>
				<input type="text" class="form-control" id="usernameInput" 
						v-model="managerOfNewRentingObject.username">
				<p v-if="usernameErrorMessage != null">{{ usernameErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label for="passwordInput">Lozinka:</label>
				<input type="password" class="form-control" id="passwordInput" 
						v-model="managerOfNewRentingObject.password">
				<p v-if="passwordErrorMessage != null">{{ passwordErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label for="passwordConfirmationInput">Ponovo unesite lozinku:</label>
				<input type="password" class="form-control" id="passwordConfirmationInput" 
						v-model="passwordConfirmation">
				<p v-if="passwordConfirmationErrorMessage != null">{{ passwordConfirmationErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label for="firstNameInput">Ime:</label>
				<input type="text" class="form-control" id="firstNameInput" 
						v-model="managerOfNewRentingObject.firstName">
				<p v-if="firstNameErrorMessage != null">{{ firstNameErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label for="lastNameInput">Prezime:</label>
				<input type="text" class="form-control" id="lastNameInput" 
						v-model="managerOfNewRentingObject.lastName">
				<p v-if="lastNameErrorMessage != null">{{ lastNameErrorMessage }}</p>
			</div>
			<div class="form-group">
				<div class="form-check form-check-inline">
					<input type="radio" class="form-check-input" 
							name="genderInputRadioGroup" id="maleGenderInput" 
							value="MALE" 
							v-model="managerOfNewRentingObject.gender">
					<label class="form-check-label" for="maleGenderInput">Muški pol</label>
				</div>
				<div class="form-check form-check-inline">
					<input type="radio" class="form-check-input" 
							name="genderInputRadioGroup" id="femaleGenderInput" 
							value="FEMALE" 
							v-model="managerOfNewRentingObject.gender">
					<label class="form-check-label" for="femaleGenderInput">Ženski pol</label>
				</div>
				<p v-if="genderErrorMessage != null">{{ genderErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label for="dateOfBirthInput">Datum rođenja:</label>
				<input type="date" class="form-control" id="dateOfBirthInput" 
						v-model="managerOfNewRentingObject.dateOfBirth">
				<p v-if="dateOfBirthErrorMessage != null">{{ dateOfBirthErrorMessage }}</p>
			</div>
			<div class="submitButtonDiv">
				<button type="submit" class="btn-default bg-primary text-white" 
						v-on:click="RegisterNewManager($event)">Registrujte novog menadžera</button>
			</div>
		</form>
	</div>
    </div>
	`,
	mounted() {
        axios.get("api/auth/logged-user").then(
            response =>	{
                this.loggedUser = response.data;
                if (this.loggedUser.role != "ADMIN") {
                    alert("Nemate pravo pristupa!");
                }

				axios.get("api/allUsers/managers-who-do-not-have-a-renting-object").then(
					response => {
						this.allManagersWhoDoNotHaveARentingObject = response.data;
					},
					error => {
						alert(error.responseText);
					}
				);
            },
            error => {
                alert(error.responseText);
            }
        );
	},
	methods: {
		CreateNewRentingObject: function(event) {
			event.preventDefault();
			console.log(this.newRentingObject);
			
			if (this.IsFormForCreatingANewRentingObjectValid()) {
				axios.post("api/locations/create-new-location", this.locationOfNewRentingObject).then(
					response => {
						console.log(response.data);

						this.newRentingObject["locationId"] = response.data["id"];

						axios.post("api/objects/create-new-renting-object", this.newRentingObject).then(
							response => {
								console.log(response.data);

								let idOfNewRentingObject = response.data["id"];
								this.managerOfNewRentingObject["rentACarObjectId"] = idOfNewRentingObject;
								console.log(this.managerOfNewRentingObject)
								axios.put("api/profiles/edit", this.managerOfNewRentingObject).then(
									response => {
										console.log(response.data);
										window.alert("Objekat uspesno dodat!");
									},
									error => {
										alert(error.responseText);
									}
								);
							},
							error => {
								alert(error.responseText)
							});
					},
					error => {
						alert(error.responseText);
					});
			}
		},
		IsFormForCreatingANewRentingObjectValid: function() {
			let formValidity = true;
			
			if (!this.IsNameOfNewRentingObjectValid()) {
				this.nameOfNewRentingObjectErrorMessage = "Ime objekta za iznajmljivanje mora započeti velikim slovom!";
				
				formValidity = false;
			} else {
				this.nameOfNewRentingObjectErrorMessage = null;
			}
			
			if (!this.IsNewRentingObjectLocationLongitudeValid()) {
				this.locationLongitudeOfNewRentingObjectErrorMessage = 
						"Geografska dužina mora biti decimalan broj iz opsega [-180, 180]!";
				
				formValidity = false;
			} else {
				this.locationLongitudeOfNewRentingObjectErrorMessage = null;
			}
			
			if (!this.IsNewRentingObjectLocationLatitudeValid()) {
				this.locationLatitudeOfNewRentingObjectErrorMessage = 
						"Geografska širina mora biti decimalan broj iz opsega [-90, 90]!";
				
				formValidity = false;
			} else {
				this.locationLatitudeOfNewRentingObjectErrorMessage = null;
			}
			
			if (!this.IsNewRentingObjectLocationAddressValid()) {
				this.locationAddressOfNewRentingObjectErrorMessage = "Adresa mora započeti velikim slovom!";
				
				formValidity = false;
			} else {
				this.locationAddressOfNewRentingObjectErrorMessage = null;
			}
			
			if (!this.IsStartHourOfNewRentingObjectValid()) {
				this.startHourOfNewRentingObjectErrorMessage = "Čas mora od 0 do 23!";
				
				formValidity = false;
			} else {
				this.startHourOfNewRentingObjectErrorMessage = null;
			}
			
			if (!this.IsStartMinuteOfNewRentingObjectValid()) {
				this.startMinuteOfNewRentingObjectErrorMessage = "Minut mora biti od 0 do 59!";
				
				formValidity = false;
			} else {
				this.startMinuteOfNewRentingObjectErrorMessage = null;
			}

			if (!this.IsEndHourOfNewRentingObjectValid()) {
				this.endHourOfNewRentingObjectErrorMessage = "Čas mora od 0 do 23!";
				
				formValidity = false;
			} else {
				this.endHourOfNewRentingObjectErrorMessage = null;
			}
			
			if (!this.IsEndMinuteOfNewRentingObjectValid()) {
				this.endMinuteOfNewRentingObjectErrorMessage = "Minut mora biti od 0 do 59!";
				
				formValidity = false;
			} else {
				this.endMinuteOfNewRentingObjectErrorMessage = null;
			}
			
			if (!this.IsLogoOfNewRentingObjectValid()) {
				this.logoOfNewRentingObjectErrorMessage = "Logo mora biti odabran!";
				
				formValidity = false;
			} else {
				this.logoOfNewRentingObjectErrorMessage = null;
			}

			if (this.managerOfNewRentingObject["id"] <= 0) {
				this.managerOfNewRentingObjectErrorMessage = "Menadžer mora biti odabran!";

				formValidity = false;
			} else {
				this.managerOfNewRentingObjectErrorMessage = null;
			}
			
			return formValidity;
		},
		IsNameOfNewRentingObjectValid: function() {
			let nameOfNewRentingObject = this.newRentingObject.name;
			console.log("Naziv: " + nameOfNewRentingObject);
			
			if (typeof nameOfNewRentingObject !== "string") {
				return false;
			}
			
			let nameRegexp = /^[A-Z\p{L}][A-Za-z\p{L}0-9 -\.]+$/;
			if (nameRegexp.test(nameOfNewRentingObject)) {
				return true;
			}
			
			return false;
		},
		IsNewRentingObjectLocationLongitudeValid: function() {
			let newRentingObjectLocationLongitude = this.locationOfNewRentingObject.longitude;
			console.log("Geografska duzina: " + newRentingObjectLocationLongitude);
			
			if (newRentingObjectLocationLongitude != null) {
				if (!isNaN(newRentingObjectLocationLongitude)) {
					if (newRentingObjectLocationLongitude >= -180 && newRentingObjectLocationLongitude <= 180) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsNewRentingObjectLocationLatitudeValid: function() {
			let newRentingObjectLocationLatitude = this.locationOfNewRentingObject.latitude;
			console.log("Geografska sirina: " + newRentingObjectLocationLatitude);
			
			if (newRentingObjectLocationLatitude != null) {
				if (!isNaN(newRentingObjectLocationLatitude)) {
					if (newRentingObjectLocationLatitude >= -90 && newRentingObjectLocationLatitude <= 90) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsNewRentingObjectLocationAddressValid: function() {
			let newRentingObjectLocationAddress = this.locationOfNewRentingObject.address;
			console.log("Adresa: " + newRentingObjectLocationAddress);

			if (typeof newRentingObjectLocationAddress !== "string") {
				return false;
			}
			
			let addressRegexp = /^[A-Z\p{L}][A-Za-z\p{L}0-9 -\.]+$/;
			if (addressRegexp.test(newRentingObjectLocationAddress)) {
				return true;
			}
			
			return false;
		},
		IsStartHourOfNewRentingObjectValid: function() {
			let startHourOfNewRentingObject = this.newRentingObject.startHour;
			console.log("Pocetak radnog vremena - cas: " + startHourOfNewRentingObject);
			
			if (startHourOfNewRentingObject != null) {
				if (!isNaN(startHourOfNewRentingObject)) {
					if (startHourOfNewRentingObject >= 0 && startHourOfNewRentingObject <= 23) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsStartMinuteOfNewRentingObjectValid: function() {
			let startMinuteOfNewRentingObject = this.newRentingObject.startMinute;
			console.log("Pocetak radnog vremena - minut: " + startMinuteOfNewRentingObject);
			
			if (startMinuteOfNewRentingObject != null) {
				if (!isNaN(startMinuteOfNewRentingObject)) {
					if (startMinuteOfNewRentingObject >= 0 && startMinuteOfNewRentingObject <= 59) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsEndHourOfNewRentingObjectValid: function() {
			let endHourOfNewRentingObject = this.newRentingObject.endHour;
			console.log("Kraj radnog vremena - cas: " + endHourOfNewRentingObject);
			
			if (endHourOfNewRentingObject != null) {
				if (!isNaN(endHourOfNewRentingObject)) {
					if (endHourOfNewRentingObject >= 0 && endHourOfNewRentingObject <= 23) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsEndMinuteOfNewRentingObjectValid: function() {
			let endMinuteOfNewRentingObject = this.newRentingObject.endMinute;
			console.log("Kraj radnog vremena - minut: " + endMinuteOfNewRentingObject);
			
			if (endMinuteOfNewRentingObject != null) {
				if (!isNaN(endMinuteOfNewRentingObject)) {
					if (endMinuteOfNewRentingObject >= 0 && endMinuteOfNewRentingObject <= 59) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsLogoOfNewRentingObjectValid: function() {
			let logoOfNewRentingObject = this.newRentingObject.image;
			console.log("Logo: " + logoOfNewRentingObject);
			
			if (typeof logoOfNewRentingObject !== "string") {
				return false;
			}
			
			return true;
		},
		ShowFormForCreatingANewManager: function() {
			this.newManagerIsNeeded = true;
		},
		RegisterNewManager: function(event) {
			event.preventDefault();
			
			if (this.IsNewManagerFormValid()) {
				axios.post("api/allUsers/register-manager", this.managerOfNewRentingObject).then(
					response => {
						console.log(response.data);

						this.allManagersWhoDoNotHaveARentingObject.push(response.data);
					},
					error => {
						alert(error.responseText);
					}
				);
			}
		},
		IsNewManagerFormValid: function() {
			let formValidity = true;
			
			if (!this.IsUsernameValid()) {
				this.usernameErrorMessage = "Korisničko ime mora početi slovom, ne " + 
						"sme sadržati razmake i specijalne znakove (osim povlake, " + 
						"donje crte i tačke)!";
				
				formValidity = false;
			} else {
				this.usernameErrorMessage = null;
			}
			
			if (!this.IsPasswordValid()) {
				this.passwordErrorMessage = "Lozinka mora sadržati između 5 i 15 znakova!";
				
				formValidity = false;
			} else {
				this.passwordErrorMessage = null;
			}
			
			if (!this.DoPasswordsMatch()) {
				this.passwordConfirmationErrorMessage = "Lozinka mora biti ponovo " + 
						"uneta!";
				
				formValidity = false;
			} else {
				this.passwordConfirmationErrorMessage = null;
			}
			
			if (!this.IsFirstNameValid()) {
				this.firstNameErrorMessage = "Ime mora započeti velikim slovom i ne " + 
						"sme sadržati brojeve!";
				
				formValidity = false;
			} else {
				this.firstNameErrorMessage = null;
			}
			
			if (!this.IsLastNameValid()) {
				this.lastNameErrorMessage = "Prezime mora započeti velikim slovom i " + 
						"ne sme sadržati brojeve!";
				
				formValidity = false;
			} else {
				this.lastNameErrorMessage = null;
			}
			
			if (!this.IsGenderValid()) {
				this.genderErrorMessage = "Pol mora biti odabran!";
				
				formValidity = false;
			} else {
				this.genderErrorMessage = null;
			}
			
			if (!this.IsDateOfBirthValid()) {
				this.dateOfBirthErrorMessage = "Datum rođenja mora biti unet!";
				
				formValidity = false;
			} else {
				this.dateOfBirthErrorMessage = null;
			}
			
			return formValidity;
		},
		IsUsernameValid: function() {
			let username = this.managerOfNewRentingObject.username;
			
			if (typeof username !== "string") {
				return false;
			}
			
			let usernameRegexp = /^[a-zA-Z][-_a-zA-Z0-9\.]+$/;
			if (usernameRegexp.test(username)) {
				return true;
			}
			
			return false;
		},
		IsPasswordValid: function() {
			let password = this.managerOfNewRentingObject.password;
			
			if (password != null) {
				if (typeof password === "string") {
					if (password.length >= 5 && password.length <= 15) {
						return true;
					}
				}
			}
			
			return false;
		},
		DoPasswordsMatch: function() {
			let passwordConfirmation = this.passwordConfirmation;
			
			if (passwordConfirmation != null) {
				if (typeof passwordConfirmation === "string") {
					if (passwordConfirmation === this.managerOfNewRentingObject.password) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsFirstNameValid: function() {
			let firstName = this.managerOfNewRentingObject.firstName;
			
			if (typeof firstName !== "string") {
				return false;
			}
			
			let nameRegexp = /^[A-Z\p{L}][a-z\p{L}]+([ -][A-Z\p{L}][a-z\p{L}]+)*$/u;
			if (nameRegexp.test(firstName)) {
				return true;
			}
			
			return false;
		},
		IsLastNameValid: function() {
			let lastName = this.managerOfNewRentingObject.lastName;
			
			if (typeof lastName !== "string") {
				return false;
			}
			
			let nameRegexp = /^[A-Z\p{L}][a-z\p{L}]+([ -][A-Z\p{L}][a-z\p{L}]+)*$/u;
			if (nameRegexp.test(lastName)) {
				return true;
			}
			
			return false;
		},
		IsGenderValid: function() {
			let gender = this.managerOfNewRentingObject.gender;
			
			if (gender != null) {
				if (typeof gender === "string") {
					if (gender === "MALE" || gender === "FEMALE") {
						return true;
					}
				}
			}
			
			return false;
		},
		IsDateOfBirthValid: function() {
			let dateOfBirth = this.managerOfNewRentingObject.dateOfBirth;
			
			if (typeof dateOfBirth !== "string") {
				return false;
			}
			
			let dateOfBirthRegexp = /^(19|20)[0-9]{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/;
			if (dateOfBirthRegexp.test(dateOfBirth)) {
				return true;
			}
			
			return false;
		}
	}
});
