Vue.component("login", {
    data:function (){
        return{
            user: {username: null, password: null},
			usernameErrorMessage: null,
			passwordErrorMessage: null
        }
    },
    template:
    `
        <div>
        	<div class="container d-flex flex-column">
		<h1>Uloguj se:</h1>
		<form role="form" id="profileForm">
			<div class="form-group">
				<label>Korisničko ime:</label>
				<input type="text" class="form-control" v-model="user.username">
				<p v-if="usernameErrorMessage!=null">{{usernameErrorMessage}}</p>
			</div>
			<div class="form-group">
				<label>Lozinka</label>
				<input type="password" class="form-control" v-model="user.password">
				<p v-if="passwordErrorMessage!=null">{{passwordErrorMessage}}</p>
			</div>
			<div class="submitButtonDiv">
				<button type="submit" class="btn-default bg-primary text-white" v-on:click="Login()">Uloguj se</button>
			</div>
		</form>
	</div>
        </div>
    `,
    mounted(){
		
    },
    methods: {
 		Login : function(){
			 event.preventDefault()
			 
			 if(this.IsFormValid()){
				 axios.post("api/auth/login", this.user).then(response=> router.push(`/profile`)).catch(error => {
        			if (error.response && error.response.status === 400) {
          				console.error("Bad Request: ", error.response.data.message);
          				window.alert("Pogresna lozinka ili korisnicko ime!");
        			}
			 })
			}
			 
		 },
		 IsFormValid: function() {
			let formValidity = true;
			
			if (!this.IsUsernameValid()) {
				this.usernameErrorMessage = "Korisničko ime mora početi slovom, ne " + 
						"sme sadržati razmake i specijalne znakove (osim povlake, " + 
						"donje crte i tačke)!";
				
				formValidity = false;
			}
			
			if (!this.IsPasswordValid()) {
				this.passwordErrorMessage = "Lozinka mora sadržati između 5 i 15 znakova!";
				
				formValidity = false;
			}
			
			return formValidity;
		},
		 IsUsernameValid: function() {
			let username = this.user.username;
			
			if (typeof username !== 'string') {
				return false;
			}
			
			let usernameRegexp = /^[a-zA-Z][-_a-zA-Z0-9\.]+$/;
			if (usernameRegexp.test(username)) {
				return true;
			}
			
			return false;
		},
		IsPasswordValid: function() {
			let password = this.user.password;
			console.log(password)
			
			if (password != null) {
				if (typeof password === 'string') {
					if (password.length >= 5 && password.length <= 15) {
						return true;
					}
				}
			}
			
			return false;
		}


    }

})
