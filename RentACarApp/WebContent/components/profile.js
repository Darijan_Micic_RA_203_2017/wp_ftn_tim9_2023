Vue.component("profile", {
    data:function (){
        return{
            customer: {id: null,logicallyDeleted:null, username: null, firstName: null, 
			lastName: null, gender: null, role: null, dateOfBirth: null,password: null },
			date: null,
			newPassword: null,
			usernameErrorMessage: null,
			passwordErrorMessage: null, 
			firstNameErrorMessage: null,
			lastNameErrorMessage: null,
			returnCustomer: {id: null,logicallyDeleted:null, username: null, firstName: null, 
			lastName: null, gender: null, role: null, dateOfBirth: null,password: null}
		
        }
    },
    template:
    `
        <div>
        	<div class="container d-flex flex-column">
		<h1>Profil:</h1>
		<form role="form" id="profileForm">
			<div class="form-group">
				<label>Korisničko ime:</label>
				<input type="text" class="form-control" v-model="customer.username">
				<p v-if="usernameErrorMessage!=null">{{usernameErrorMessage}}</p>
			</div>
			<div class="form-group">
				<label>Ime:</label>
				<input type="text" class="form-control" v-model="customer.firstName">
				<p v-if="firstNameErrorMessage!=null">{{firstNameErrorMessage}}</p>
			</div>
			<div class="form-group">
				<label>Prezime:</label>
				<input type="text" class="form-control" v-model="customer.lastName">
				<p v-if="lastNameErrorMessage!=null">{{lastNameErrorMessage}}</p>
			</div>
			<div class="form-group">
				<div class="form-check form-check-inline">
					<input type="radio" class="form-check-input" name="genderInputRadioGroup" 
						id="maleGenderInput" value="MALE" v-model="customer.gender" >
					<label class="form-check-label" for="maleGenderInput">Muški pol</label>
				</div>
				<div class="form-check form-check-inline">
					<input type="radio" class="form-check-input" name="genderInputRadioGroup" 
						id="femaleGenderInput" value="FEMALE" v-model="customer.gender">
					<label class="form-check-label" for="femaleGenderInput">Ženski pol</label>
				</div>
			</div>
			<div class="form-group">
				<label>Datum rođenja:</label>
				<input type="date" class="form-control" v-model="date">
			</div>
			<div class="form-group">
				<label>Promeni lozinku:</label>
				<input type="password" class="form-control" v-model="newPassword">
				<p v-if="passwordErrorMessage!=null">{{passwordErrorMessage}}</p>
			</div>
			<div class="submitButtonDiv">
				<button type="submit" class="btn-default bg-primary text-white" 
						v-on:click="EditProfile($event)">Promeni podatke</button>
			</div>
		</form>
	</div>
        </div>
    `,
    mounted(){
		axios.get("api/auth/logged-user")
			.then(response =>	
				{
					this.customer = response.data
					
					if(this.customer===""){
						window.alert("Nemate pravo pristupa!")
						router.push(`/login`)
					}
					
					}).then(response=>{
					this.date = new Date(this.customer.dateOfBirth).toISOString().slice(0, 10)
					
					
					})
    },
    methods: {
		EditProfile: function(event) {
			 event.preventDefault();
			 
			 this.customer.dateOfBirth = this.date
			 if(this.newPassword != null){
				 this.customer.password = this.newPassword
			 }
			 
			 
			 
			if (this.IsFormValid()) {
				console.log(this.customer)
				axios.put("api/profiles/edit", this.customer);
			}
		 },
		 IsFormValid: function() {
			let formValidity = true;
			
			if (!this.IsUsernameValid()) {
				this.usernameErrorMessage = "Korisničko ime mora početi slovom, ne " + 
						"sme sadržati razmake i specijalne znakove (osim povlake, " + 
						"donje crte i tačke)!";
				
				formValidity = false;
			}
			
			if (!this.IsPasswordValid()) {
				this.passwordErrorMessage = "Lozinka mora sadržati između 5 i 15 znakova!";
				
				formValidity = false;
			}
			
			
			if (!this.IsFirstNameValid()) {
				this.firstNameErrorMessage = "Ime mora započeti velikim slovom i ne " + 
						"sme sadržati brojeve!";
				
				formValidity = false;
			}
			
			if (!this.IsLastNameValid()) {
				this.lastNameErrorMessage = "Prezime mora započeti velikim slovom i " + 
						"ne sme sadržati brojeve!";
				
				formValidity = false;
			}

			return formValidity;
		},
		 IsUsernameValid: function() {
			let username = this.customer.username;
			
			if (typeof username !== 'string') {
				return false;
			}
			
			let usernameRegexp = /^[a-zA-Z][-_a-zA-Z0-9\.]+$/;
			if (usernameRegexp.test(username)) {
				return true;
			}
			
			return false;
		},
		IsPasswordValid: function() {
			let password = this.customer.password;
			console.log(password)
			
			if (password != null) {
				if (typeof password === 'string') {
					if (password.length >= 5 && password.length <= 15) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsFirstNameValid: function() {
			let firstName = this.customer.firstName;
			
			if (typeof firstName !== 'string') {
				return false;
			}
			
			let nameRegexp = /^[A-Z\p{L}][a-z\p{L}]+([ -][A-Z\p{L}][a-z\p{L}]+)*$/u;
			if (nameRegexp.test(firstName)) {
				return true;
			}
			
			return false;
		},
		IsLastNameValid: function() {
			let lastName = this.customer.lastName;
			
			if (typeof lastName !== 'string') {
				return false;
			}
			
			let nameRegexp = /^[A-Z\p{L}][a-z\p{L}]+([ -][A-Z\p{L}][a-z\p{L}]+)*$/u;
			if (nameRegexp.test(lastName)) {
				return true;
			}
			
			return false;
		}



    }

})
