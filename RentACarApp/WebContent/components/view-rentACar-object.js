Vue.component("rentACar-object", {
    data:function (){
        return{
         	object: {id: null,logicallyDeleted:null, name: null, startHour: null, startMinute: null, endHour: null, endMinute: null, status: null, locationId: null, image: null, grade: null, cars: null},
         	pathId: null,
         	workingHours: null,
         	location: {id: null, longitude: null, latitude: null, address: null},
			cars: [],
			comments: null,
			commentUsers: [],
			loggedUser: null,
			commentsCustomers: null
        }
    },
    template:
    `
        <div >
        	<div class="container d-flex flex-column">
		<h1>Rent A Car Objekat: <img :src="object.image"></h1>
			<div class="form-group">
				<label>Ime:</label>
				<input type="text" class="form-control" v-model="object.name" disabled>
			</div>
			<div class="form-group">
				<label>Radno vreme:</label>
				<input type="text" class="form-control" v-model="workingHours" disabled>
			</div>
			<div class="form-group">
				<label>Status:</label>
				<input type="text" class="form-control" v-model="object.status" disabled>
			</div>
			<div class="form-group">
				<label v-if="object.grade != 0">Ocena:</label>
				<input v-if="object.grade != 0" type="text" class="form-control" v-model="object.grade" disabled>
			</div>
			<div class="form-group">
				<label>Adresa:</label>
				<input type="text" class="form-control" v-model="location.address" disabled>
			</div>
			<div class="form-group">
				<label>Longituda:</label>
				<input type="text" class="form-control" v-model="location.longitude" disabled>
			</div>
			<div class="form-group">
				<label>Latituda:</label>
				<input type="text" class="form-control" v-model="location.latitude" disabled>
			</div>
			<div class="form-group">
			<br>
				<h3>Kola:</h3>
				<table class="table">
				<thead>
		    		<tr>
		    			<th>Brend</th>
		    			<th>Cena</th>
		    			<th>Tip</th>
		    			<th>Vrsta</th>
		    			<th>Gorivo</th>
		    			<th>Potrosnja</th>
		    			<th>Broj vrata</th>
		    			<th>Max putnika</th>
		    			<th>Opis</th>
		    			<th>Slika</th>
		    			<th>Status</th>
		    		</tr>
		    	</thead>
		    	<tbody>	
		    		<tr v-for="car in cars">
		    			<td v-if="!car.logicallyDeleted">{{car.brand}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.price}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.type}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.kind}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.fuel}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.consumption}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.noDoors}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.noPeople}}</td>
		    			<td v-if="!car.logicallyDeleted">{{car.description}}</td>
		    			<td v-if="!car.logicallyDeleted"><img :src="car.image"></td>
		    			<td v-if="!car.logicallyDeleted">{{car.status}}</td>
		    		</tr>
		    	</tbody>
	    	</table>
	    	<br>
	    	<div v-if="loggedUser.role === 'ADMIN' || loggedUser.role === 'MANAGER'">
	    	<h3 v-if="commentUsers.length !== 0">Komentari:</h3>
				<table class="table" v-if="commentUsers.length !== 0">
				<thead>
		    		<tr>
		    			<th>Korisnicko ime korisnika</th>
		    			<th>Komentar</th>
		    			<th>Ocena</th>
		    		</tr>
		    	</thead>
		    	<tbody>	
		    		<tr v-for="(c, index) in comments">
		    			<td>{{commentUsers[index].username}}</td>
		    			<td>{{c.comment}}</td>
		    			<td>{{c.rating}}</td>
		    		</tr>
		    	</tbody>
	    	</table>
	    	</div>
	    	<div v-if="loggedUser.role === 'CUSTOMER'">
	    	<h3 v-if="commentUsers.length !== 0">Komentari:</h3>
				<table class="table" v-if="commentUsers.length !== 0">
				<thead>
		    		<tr>
		    			<th>Korisnicko ime korisnika</th>
		    			<th>Komentar</th>
		    			<th>Ocena</th>
		    		</tr>
		    	</thead>
		    	<tbody>	
		    		<tr v-for="(c, index) in commentsCustomers">
		    			<td>{{commentUsers[index].username}}</td>
		    			<td>{{c.comment}}</td>
		    			<td>{{c.rating}}</td>
		    		</tr>
		    	</tbody>
	    	</table>
	    	</div>
			
	</div>
        </div>
    `,
    mounted(){
		this.pathId = this.$route.params.id
		
		axios.get("api/auth/logged-user")
			.then(response =>	
				this.loggedUser = response.data)
		
		axios.get("api/objects/" + this.pathId).then(response=>this.object = response.data)
			.then(response=> this.workingHours = this.object.startHour.toString() + ":" + this.object.startMinute.toString()
			 + " - " + this.object.endHour.toString() + ":" + this.object.endMinute.toString())
			.then(response=>axios.get("api/locations/" + this.object.locationId).then(response=> this.location = response.data))
			.then(response=>{
				for(let item of this.object.cars){
					axios.get("api/cars/" + item).then(response=>this.cars.push(response.data))
				}})
			.then(response=> axios.get("api/comments/" + this.object.id).then(response=> this.commentsCustomers = response.data))
			.then(response=> axios.get("api/comments/acceptedAndRejectedComments/" + this.object.id).then(response=> this.comments = response.data))
			.then(response=> {
				for(let item of this.comments){
					axios.get("api/profiles/" + item.userId).then(response => this.commentUsers.push(response.data))
				}
			})
		
	}
		,
    methods: {
 		
    }

})
