Vue.component("customer-view-rentings", {
    data:function (){
        return{
         	object: {id: null, name: null, startHour: null, startMinute: null, endHour: null, endMinute: null, status: null, locationId: null, image: null, grade: null, cars: null},
         	pathId: null,
         	workingHours: null,
			cars: [],
			comments: null,
			commentUsers: [],
			loggedUser: null,
			rentings: null,
			rentingUsers: [],
			uniqueUsers: [],
			carsFromRenting: [],
        }
    },
    template:
    `
       <div v-if="loggedUser.role === 'CUSTOMER'">
	    	<h3>Narudzbine:</h3>
				<table class="table">
				<thead>
		    		<tr>
		    			<th>Sifra</th>
		    			<th>Kola</th>
		    			<th>Datum</th>
		    			<th>Trajanje</th>
		    			<th>Cena</th>
		    			<th>Status</th>
		    			<th></th>
		    		</tr>
		    	</thead>
		    	<tbody>	
		    		<tr v-for="(c, index) in rentings">
		    			<td>{{c.id}}</td>
		    			<td>{{carsFromRenting[index][0].brand}}<label v-if="carsFromRenting[index][1]!==undefined"> ;{{carsFromRenting[index][1].brand}}</label></td>
		    			<td>{{c.date}}</td>
		    			<td>{{c.length}}</td>
		    			<td>{{c.price}}</td>
		    			<td>{{c.status}}</td>
		    			<td v-if="c.status==='RETURNED'"><button v-on:click="Oceni(c.rentACarObjectId, c.customerId)">Oceni</button></td>
		    			<td v-if="c.status!=='RETURNED'"></td>
		    		</tr>
		    	</tbody>
	    	</table>
        </div>
    `,
    mounted(){
			axios.get("api/auth/logged-user")
			.then(response =>{
				this.loggedUser = response.data
				
				
				axios.get("api/rentings/forCustomer/" + this.loggedUser.id).then(response=>
				{
					this.rentings = response.data
					const ids = []
					for (let i = 0; i < this.rentings.length; i++) {
        				const renting = this.rentings[i];
        				ids.push(renting.customerId)
        				axios.get("api/allUsers/getCustomer/" + renting.customerId).then(response=>
        				{
							this.rentingUsers.push(response.data)
							
						})
        			}
        			const uniqueIds = [...new Set(ids)]
        			for (let i = 0; i < uniqueIds.length; i++) {
        				const id = uniqueIds[i];
        				axios.get("api/allUsers/getCustomer/" + id).then(response=>
        				{
							this.uniqueUsers.push(response.data)
							
						})
        			}
        			const carIds = []
        			for (let i = 0; i < this.rentings.length; i++) {
						
        				const renting = this.rentings[i];
        				const carIdsRenting = []
        				for(let i = 0; i < renting.carsIds.length; i++){
							
							const carId = renting.carsIds[i]
							axios.get("api/cars/" + carId).then(response=>carIdsRenting.push(response.data))
						}
        				carIds.push(carIdsRenting)
        			}
        			this.carsFromRenting = carIds;  
				})
				
				axios.get("api/objects/" + this.loggedUser.rentACarObjectId).then(response=>
				{
					this.object = response.data
					this.workingHours = this.object.startHour.toString() + ":" + this.object.startMinute.toString()
			 			+ " - " + this.object.endHour.toString() + ":" + this.object.endMinute.toString()
			 		axios.get("api/locations/" + this.object.locationId).then(response=> 
			 		{
						 this.location = response.data
					 })
					for(let item of this.object.cars){
					axios.get("api/cars/" + item).then(response=>this.cars.push(response.data))
					}
				})
			})
			
			axios.get("api/auth/logged-user")
			.then(response =>	
				this.loggedUser = response.data).then(response=> {
					if(this.loggedUser.role!=="CUSTOMER"){
						window.alert("Nemate pravo pristupa!")
					}
				})
		
	}
		,
    methods: {
 		Oceni : function(rentACarObjectId, customerId){
			 this.$router.push(`/leaveComment/${rentACarObjectId}/${customerId}`);
		 }
    }

})
