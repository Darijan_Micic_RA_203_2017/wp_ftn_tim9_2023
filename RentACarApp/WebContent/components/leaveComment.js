Vue.component("leaveComment", {
    data:function (){
        return{
            comment: {userId: null, rentACarObjectId: null, comment: null, rating: null, status: null},
            error: null
        }
    },
    template:
    `
        <div>
        	<div class="container d-flex flex-column">
		<h1>Profil:</h1>
		<form role="form" id="profileForm">
			<div class="form-group">
				<label>Komentar:</label>
				<input type="text" class="form-control" v-model="comment.comment">
			</div>
			<div class="form-group">
				<label>Ocena:</label>
				<input type="number" class="form-control" v-model="comment.rating">
			</div>
			<p v-if="error!=null">{{error}}</p>
			<div class="submitButtonDiv">
				<button type="submit" class="btn-default bg-primary text-white" v-on:click="Dodaj()">Dodaj komentar</button>
			</div>
		</form>
	</div>
        </div>
    `,
    mounted(){
		
    },
    methods: {
 		Dodaj : function(){
			 event.preventDefault()
			 
			 this.comment.userId = this.$route.params.customerId
			 this.comment.rentACarObjectId = this.$route.params.objectId
			 this.comment.status = "PENDING"
	
			 if(this.IsFormValid()){
				 axios.post("api/comments/addComment", this.comment).then(response=> window.alert("Komentar dodat uspesno!"))
			 }else{
				 
			 }
			 
			 
		 },
		 IsFormValid: function() {
			let formValidity = true;
			
			if (!this.IsRatingValid()) {
				this.error = "Ocena mora biti od 1 do 5";
				
				formValidity = false;
			}
			
			if(!(this.comment.comment!=null && this.comment.rating!=null)){
				this.error = "Komentar i ocena se moraju popuniti!"
				formValidity = false;
			}
			
			return formValidity;
		},
		 IsRatingValid: function() {
			let rating = this.comment.rating;
			
			if (parseInt(rating) <= 5 && parseInt(rating) >=1) {
				return true;
			}
			
			return false;
		}

    }

})
