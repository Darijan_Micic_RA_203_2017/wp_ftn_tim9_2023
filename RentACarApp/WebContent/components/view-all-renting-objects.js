Vue.component("view-all-renting-objects", {
	data: function() {
		return {
			allCars: [],
			allLocations: [],
			allRentingObjects: [],
			sortedRentingObjects: [],
			sortOrder: "ascending",
			filteredCarKind: "",
         	filteredFuelType: "",
			isRentingObjectOpen: "",
			searchParams: {
				name: "",
				carType: "",
				location: "",
				grade: ""
			}
		}
	},
	template: `
	<div class="container d-flex flex-column">
		<h1>Svi objekti za iznajmljivanje automobila:</h1>
		<!--
		Pretraga po: nazivu, tipu vozila, lokaciji i prosecnoj oceni.
		Sortiranje po: nazivu, lokaciji, prosecnoj oceni.
		Filtriranje po: vrsta vozila, tip goriva vozila, prikaz samo otvorenih objekata.
		-->
		<div class="button-container">
			<button type="button" class="inline-button" v-on:click="SortByName">Sortiraj po nazivu</button>
			<button type="button" class="inline-button" v-on:click="SortByLocation">Sortiraj po lokaciji</button>
			<button type="button" class="inline-button" v-on:click="SortByAverageGrade">Sortiraj po prosečnoj oceni</button>
		</div>
		<div class="form-group">
			<br>
			<label for="carKindFilter">Filtriraj po vrsti vozila:</label>
			<select id="carKindFilter" v-model="filteredCarKind">
				<option value="">Sve vrste</option>
				<option value="MANUEL">Ručni menjač</option>
				<option value="AUTOMATIC">Automatski menjač</option>
			</select>
			<label for="fuelTypeFilter">Filtriraj po vrsti goriva:</label>
			<select id="fuelTypeFilter" v-model="filteredFuelType">
				<option value="">Sva goriva</option>
				<option value="DISEL">Dizel</option>
				<option value="BENZIN">Benzin</option>
				<option value="HYBRID">Hibrid</option>
				<option value="ELECTRIC">Struja</option>
			</select>
			<label for="rentingObjectStatusFilter">Filtriraj po otvorenosti objekta:</label>
			<select id="rentingObjectStatusFilter" v-model="isRentingObjectOpen">
				<option value=""></option>
				<option value="WORKING">Otvoreni</option>
				<option value="NOTWORKING">Zatvoreni</option>
			</select>
		</div>
		<div class="form-group">
			<br>
			<label for="nameInput">Naziv:</label>
			<input class="search-input" type="text" id="nameInput" v-model="searchParams.name">
			<label for="carTypeInput">Tip vozila:</label>
			<input class="search-input" type="text" id="carTypeInput" v-model="searchParams.carType">
			<label for="locationInput">Lokacija (grad ili država):</label>
			<input class="search-input" type="text" id="locationInput" v-model="searchParams.location">
			<label for="averageGradeInput">Prosečna ocena:</label>
			<input class="search-input" type="number" id="averageGradeInput" v-model="searchParams.grade">
			<button type="button" class="inline-button" v-on:click="SearchRentingObjects">Pretraži</button>
		</div>
		<br>
		<table class="table table-dark table-bordered border-primary" id="rentingObjectsTable">
			<thead class="table-primary table-bordered border-primary">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Naziv</th>
					<th scope="col">Lokacija</th>
					<th scope="col">Logo</th>
					<th scope="col">Prosečna ocena</th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="obj of ShowFilteredRentingObjects" v-on:click="GoToRentingObjectWithId(obj.id)">
					<th scope="row">{{ obj.id }}</th>
					<td class="nameTableData">{{ obj.name }}</td>
					<td class="locationTableData">{{ ResolveLocation(obj.locationId) }}</td>
					<td class="logoTableData"><img :src="obj.image"></td>
					<td class="averageGradeTableData">{{ obj.grade }}</td>
				</tr>
			</tbody>
		</table>
	</div>
	`,
	mounted() {
		axios.get("api/cars").then(
			response => {
				this.allCars = response.data;
			},
			error => {
				alert(error.responseText);
			}
		);
		axios.get("api/locations").then(
			response => {
				this.allLocations = response.data;
			},
			error => {
				alert(error.responseText);
			}
		);

		axios.get("api/objects/all-non-deleted").then(
			response => {
				this.allRentingObjects = response.data;
				this.sortedRentingObjects = response.data;
			},
			error => {
				alert(error.responseText);
			}
		);
	},
	computed: {
		ShowFilteredRentingObjects: function() {
			let filteredRentingObjects = this.sortedRentingObjects; // start with all sorted renting objects
			
			if (this.filteredCarKind) {
				filteredRentingObjects = filteredRentingObjects.filter(obj => 
						this.DoesObjectHaveACarOfMatchingKind(obj) == true);
			}
			
			if (this.filteredFuelType) {
				filteredRentingObjects = filteredRentingObjects.filter(obj => 
						this.DoesObjectHaveACarOfMathingFuelType(obj) == true);
			}
			
			if (this.isRentingObjectOpen) {
				filteredRentingObjects = filteredRentingObjects.filter(obj => obj["status"] == this.isRentingObjectOpen);
			}

			return filteredRentingObjects;
		}
	},
	methods: {
		ResolveLocation: function(locationId) {
			let locationCard;
			for (let l of this.allLocations) {
				if (l['id'] == locationId) {
					locationCard = this.CreateLocationCard(l);

					return locationCard;
				}
			}
		},
		CreateLocationCard: function(location) {
			//let locationCard = document.createElement("div");
			//locationCard.classList.add("card", "text-center", "text-white", "bg-dark", "border-light");

			//let locationCardBody = document.createElement("div");
			//locationCardBody.classList.add("card-body");
			
			//let locationCardTitle = document.createElement("h5");
			//locationCardTitle.classList.add("card-title");
			//locationCardTitle.textContent = location["address"];
			let locationCard = "";
			locationCard = locationCard.concat(location["address"], "\n");
			
			//let locationCardText = document.createElement("p");
			//locationCardText.classList.add('card-text');
			//locationCardText.textContent = location["latitude"] + " " + location["longitude"];
			locationCard = locationCard.concat("[", location["latitude"], "; ", location["longitude"], "]");
			
			//locationCardBody.append(locationCardTitle);
			//locationCardBody.append(locationCardText);

			//locationCard.append(locationCardBody);
			console.log(locationCard);

			return locationCard;
		},
		ResolveLogo: function(logoPath) {
			let logoElement = document.createElement("img");
			logoElement.classList.add("img-fluid", "rounded", "mx-auto", "d-block");
			logoElement.setAttribute("src", logoPath);
			logoElement.setAttribute("alt", "logo");
			
			return logoElement;
		},
		SortByName: function() {
			this.sortOrder = this.sortOrder === "ascending" ? "descending" : "ascending";
			this.sortedRentingObjects.sort((a, b) => {
				const nameA = a["name"].toUpperCase();
				const nameB = b["name"].toUpperCase();

				if (nameA < nameB) {
					return this.sortOrder === 'ascending' ? -1 : 1;
				}
				if (nameA > nameB) {
					return this.sortOrder === 'ascending' ? 1 : -1;
				}
				
				return 0;
		    });
		},
		SortByLocation: function() {
			this.sortOrder = this.sortOrder === "ascending" ? "descending" : "ascending";
			this.sortedRentingObjects.sort((a, b) => {
				const locationAId = a["locationId"];
				let locationA = "";
				for (let l of this.allLocations) {
					if (l["id"] == locationAId) {
						locationA = l["address"].toUpperCase();
					}
				}
				const locationBId = b["locationId"];
				let locationB = "";
				for (let l of this.allLocations) {
					if (l["id"] == locationBId) {
						locationB = l["address"].toUpperCase();
					}
				}
				
				if (locationA < locationB) {
					return this.sortOrder === 'ascending' ? -1 : 1;
				}
				if (locationA > locationB) {
					return this.sortOrder === 'ascending' ? 1 : -1;
				}
				
				return 0;
		    });
		},
		SortByAverageGrade: function() {
			this.sortOrder = this.sortOrder === "ascending" ? "descending" : "ascending";
			this.sortedRentingObjects.sort((a, b) => {
				const gradeA = a["grade"];
				const gradeB = b["grade"];
				
				if (gradeA < gradeB) {
					return this.sortOrder === 'ascending' ? -1 : 1;
				}
				if (gradeA > gradeB) {
					return this.sortOrder === 'ascending' ? 1 : -1;
				}
				
				return 0;
		    });
		},
		DoesObjectHaveACarOfMatchingKind: function(rentingObject) {
			for (let car of this.allCars) {
				if (rentingObject["cars"].includes(car["id"]) && car["kind"] == this.filteredCarKind) {
					return true;
				}
			}

			return false;
		},
		DoesObjectHaveACarOfMathingFuelType: function(rentingobject) {
			for (let car of this.allCars) {
				if (rentingobject["cars"].includes(car["id"]) && car["fuel"] == this.filteredFuelType) {
					return true;
				}
			}

			return false;
		},
		SearchRentingObjects: function() {
			axios.get("api/objects/search", { params: this.searchParams }).then(
				response => {
					this.sortedRentingObjects = response.data;
				},
				error => {
					alert(error.responseText);
				}
			);
		},
		GoToRentingObjectWithId: function(rentingObjectId) {
			this.$router.push(`/rentACar/${rentingObjectId}`);
		}
	}
});
