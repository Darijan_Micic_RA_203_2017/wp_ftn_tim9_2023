Vue.component("add-car", {
    data: function() {
        return {
			loggedUser: null,
			newCar: {
				id: 0,
				logicallyDeleted: false,
				brand: null,
				price: null,
				type: null,
				rentACarObjectId: 0,
				kind: null,
				fuel: null,
				consumption: null,
				noDoors: null,
				noPeople: null,
				description: null,
				image: null,
				status: "FREE"
			},
			brandOfNewCarErrorMessage: null,
			priceOfNewCarErrorMessage: null,
			typeOfNewCarErrorMessage: null,
			kindOfNewCarErrorMessage: null,
			fuelTypeOfNewCarErrorMessage: null,
			consumptionOfNewCarErrorMessage: null,
			numberOfDoorsOfNewCarErrorMessage: null,
			numberOfPeopleOfNewCarErrorMessage: null,
			descriptionOfNewCarErrorMessage: null,
			imageOfNewCarErrorMessage: null
        }
    },
    template:
    `
    <div v-if="loggedUser.role == 'MANAGER'">
    <div class="container d-flex flex-column">
        <h1>Novi automobil:</h1>
		<form role="form" id="newCarForm">
			<div class="form-group">
				<label>Brend:</label>
				<input type="text" class="form-control" v-model="newCar.brand">
				<p v-if="brandOfNewCarErrorMessage != null">{{ brandOfNewCarErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label>Cena:</label>
				<input type="number" class="form-control" v-model="newCar.price">
				<p v-if="priceOfNewCarErrorMessage != null">{{ priceOfNewCarErrorMessage }}</p>
			</div>
			<div class="form-group">
                <label>Tip:</label>
                <select class="form-control" v-model="newCar.type">
                    <option value=""></option>
                    <option value="AUTO">Auto</option>
                    <option value="COMBI">Kombi</option>
                    <option value="MOBILEHOME">Kamper</option>
                </select>
				<p v-if="typeOfNewCarErrorMessage != null">{{ typeOfNewCarErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label>Vrsta:</label>
				<select class="form-control" v-model="newCar.kind">
                    <option value=""></option>
			        <option value="MANUEL">Rucni menjac</option>
			        <option value="AUTOMATIC">Automatski menjac</option>
			    </select>
				<p v-if="kindOfNewCarErrorMessage != null">{{ kindOfNewCarErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label>Tip goriva:</label>
				<select class="form-control" v-model="newCar.fuel">
					<option value=""></option>
					<option value="BENZIN">Benzin</option>
					<option value="DISEL">Dizel</option>
			        <option value="HYBRID">Hibrid</option>
			        <option value="ELECTRIC">Struja</option>
			    </select>
				<p v-if="fuelTypeOfNewCarErrorMessage != null">{{ fuelTypeOfNewCarErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label>Potrosnja:</label>
				<input type="number" class="form-control" v-model="newCar.consumption">
				<p v-if="consumptionOfNewCarErrorMessage != null">{{ consumptionOfNewCarErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label>Broj vrata:</label>
				<input type="number" class="form-control" v-model="newCar.noDoors">
				<p v-if="numberOfDoorsOfNewCarErrorMessage != null">{{ numberOfDoorsOfNewCarErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label>Broj osoba:</label>
				<input type="number" class="form-control" v-model="newCar.noPeople">
				<p v-if="numberOfPeopleOfNewCarErrorMessage != null">{{ numberOfPeopleOfNewCarErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label>Opis:</label>
				<input type="text" class="form-control" v-model="newCar.description">
				<p v-if="descriptionOfNewCarErrorMessage != null">{{ descriptionOfNewCarErrorMessage }}</p>
			</div>
			<div class="form-group">
				<label>Slika:</label>
				<img v-bind:src="newCar.image" class="form-control">
				<select class="form-control" v-model="newCar.image">
                    <option value="images/Mercedes_E_klasa.jpeg">Mercedes E</option>
			        <option value="images/car2.jpg">Crvena kola</option>
			        <option value="images/car1.jpg">Plava kola</option>
			    </select>
				<p v-if="imageOfNewCarErrorMessage != null">{{ imageOfNewCarErrorMessage }}</p>
			</div>
			<div class="submitButtonDiv">
				<button type="submit" class="btn-default bg-primary text-white" 
                        v-on:click="AddNewCar($event)">
                    Dodajte novi automobil
                </button>
			</div>
		</form>
	</div>
    </div>
    `,
    mounted() {
		axios.get("api/auth/logged-user").then(
            response =>	{
                this.loggedUser = response.data;
                if (this.loggedUser.role != "MANAGER") {
                    alert("Nemate pravo pristupa!");
                }

				this.newCar["rentACarObjectId"] = this.loggedUser["rentACarObjectId"];
            },
            error => {
                alert(error.responseText);
            }
        );
	},
    methods: {
		AddNewCar: function(event) {
			event.preventDefault();
			console.log(this.newCar);
            
			if (this.IsFormForAddingANewCarValid()) {
				axios.post("api/cars/create-new-car", this.newCar).then(
					response => {
						console.log(response.data);

						this.$router.push("/managerObject");
					},
					error => {
						alert(error.message);
					}
				);
			}
        },
		IsFormForAddingANewCarValid: function() {
			let formValidity = true;
			
			if (!this.IsBrandOfNewCarValid()) {
				this.brandOfNewCarErrorMessage = "Brend automobila mora započeti velikim slovom!";
				
				formValidity = false;
			} else {
				this.brandOfNewCarErrorMessage = null;
			}
			
			if (!this.IsPriceOfNewCarValid()) {
				this.priceOfNewCarErrorMessage = "Cena iznajmljivanja mora biti pozitivan decimalan broj!";
				
				formValidity = false;
			} else {
				this.priceOfNewCarErrorMessage = null;
			}
			
			if (!this.IsTypeOfNewCarValid()) {
				this.typeOfNewCarErrorMessage = "Tip mora biti odabran!";
				
				formValidity = false;
			} else {
				this.typeOfNewCarErrorMessage = null;
			}
			
			if (!this.IsKindOfNewCarValid()) {
				this.kindOfNewCarErrorMessage = "Vrsta mora biti odabrana!";
				
				formValidity = false;
			} else {
				this.kindOfNewCarErrorMessage = null;
			}

			if (!this.IsFuelTypeOfNewCarValid()) {
				this.fuelTypeOfNewCarErrorMessage = "Tip goriva mora biti odabrana!";
				
				formValidity = false;
			} else {
				this.fuelTypeOfNewCarErrorMessage = null;
			}

			if (!this.IsConsumptionOfNewCarValid()) {
				this.consumptionOfNewCarErrorMessage = "Potrosnja mora biti pozitivan celi broj!";
				
				formValidity = false;
			} else {
				this.consumptionOfNewCarErrorMessage = null;
			}
			
			if (!this.IsNumberOfDoorsOfNewCarValid()) {
				this.numberOfDoorsOfNewCarErrorMessage = "Broj vrata mora biti izmedju 2 i 7!";
				
				formValidity = false;
			} else {
				this.numberOfDoorsOfNewCarErrorMessage = null;
			}
			
			if (!this.IsNumberOfPeopleOfNewCarValid()) {
				this.numberOfPeopleOfNewCarErrorMessage = "Broj osoba mora biti najmanje 1!";
				
				formValidity = false;
			} else {
				this.numberOfPeopleOfNewCarErrorMessage = null;
			}

			if (!this.IsDescriptionOfNewCarValid()) {
				this.descriptionOfNewCarErrorMessage = "Opis mora biti unet!";
				
				formValidity = false;
			} else {
				this.descriptionOfNewCarErrorMessage = null;
			}
			
			if (!this.IsImageOfNewCarValid()) {
				this.imageOfNewCarErrorMessage = "Slika mora biti odabrana!";
				
				formValidity = false;
			} else {
				this.imageOfNewCarErrorMessage = null;
			}
			
			return formValidity;
		},
		IsBrandOfNewCarValid: function() {
			let brandOfNewCar = this.newCar.brand;
			console.log("Brend: " + brandOfNewCar);
			
			if (typeof brandOfNewCar !== "string") {
				return false;
			}
			
			let brandRegexp = /^[A-Z\p{L}][A-Za-z\p{L}0-9 -\.]+$/;
			if (brandRegexp.test(brandOfNewCar)) {
				return true;
			}
			
			return false;
		},
		IsPriceOfNewCarValid: function() {
			let priceOfNewCar = this.newCar.price;
			console.log("Cena: " + priceOfNewCar);
			
			if (priceOfNewCar != null) {
				if (!isNaN(priceOfNewCar)) {
					if (priceOfNewCar > 0) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsTypeOfNewCarValid: function() {
			let typeOfNewCar = this.newCar.type;
			console.log("Tip: " + typeOfNewCar);
			
			if (typeOfNewCar != null) {
				if (typeof typeOfNewCar === "string") {
					if (typeOfNewCar === "AUTO" || typeOfNewCar === "COMBI" || typeOfNewCar === "MOBILEHOME") {
						return true;
					}
				}
			}
			
			return true;
		},
		IsKindOfNewCarValid: function() {
			let kindOfNewCar = this.newCar.kind;
			console.log("Vrsta: " + kindOfNewCar);
			
			if (kindOfNewCar != null) {
				if (typeof kindOfNewCar === "string") {
					if (kindOfNewCar === "MANUEL" || kindOfNewCar === "AUTOMATIC") {
						return true;
					}
				}
			}
			
			return true;
		},
		IsFuelTypeOfNewCarValid: function() {
			let fuelTypeOfNewCar = this.newCar.fuel;
			console.log("Tip goriva: " + fuelTypeOfNewCar);
			
			if (fuelTypeOfNewCar != null) {
				if (typeof fuelTypeOfNewCar === "string") {
					if (fuelTypeOfNewCar === "BENZIN" || fuelTypeOfNewCar === "DISEL" || fuelTypeOfNewCar === "HYBRID" 
							|| fuelTypeOfNewCar === "ELECTRIC") {
						return true;
					}
				}
			}
			
			return true;
		},
		IsConsumptionOfNewCarValid: function() {
			let consumptionOfNewCar = this.newCar.consumption;
			console.log("Potrosnja: " + consumptionOfNewCar);
			
			if (consumptionOfNewCar != null) {
				if (!isNaN(consumptionOfNewCar)) {
					if (consumptionOfNewCar >= 1) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsNumberOfDoorsOfNewCarValid: function() {
			let numberOfDoorsOfNewCar = this.newCar.noDoors;
			console.log("Broj vrata: " + numberOfDoorsOfNewCar);
			
			if (numberOfDoorsOfNewCar != null) {
				if (!isNaN(numberOfDoorsOfNewCar)) {
					if (numberOfDoorsOfNewCar >= 2 && numberOfDoorsOfNewCar <= 7) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsNumberOfPeopleOfNewCarValid: function() {
			let numberOfPeopleOfNewCar = this.newCar.noPeople;
			console.log("Broj osoba: " + numberOfPeopleOfNewCar);
			
			if (numberOfPeopleOfNewCar != null) {
				if (!isNaN(numberOfPeopleOfNewCar)) {
					if (numberOfPeopleOfNewCar >= 1) {
						return true;
					}
				}
			}
			
			return false;
		},
		IsDescriptionOfNewCarValid: function() {
			let descriptionOfNewCar = this.newCar.description;
			console.log("Opis: " + descriptionOfNewCar);
			
			if (!descriptionOfNewCar) {
				return false;
			}
			
			return true;
		},
		IsImageOfNewCarValid: function() {
			let imageOfNewCar = this.newCar.image;
			console.log("Slika: " + imageOfNewCar);
			
			if (!imageOfNewCar) {
				return false;
			}
			
			return true;
		}
    }
});
