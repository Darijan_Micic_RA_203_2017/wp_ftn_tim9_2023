package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.CarType;
import beans.Location;
import beans.Role;
import dao.CarDAO;
import dao.LocationDAO;
import dao.ProfileDAO;
import dao.RentACarDAO;
import dao.UsersDAO;
import dtos.AllUsersDTO;
import dtos.CarDTO;
import dtos.RentACarDTO;
import dtos.UserDTO;
import validators.RentingObjectValidator;
import validators.UserValidator;

@Path("/objects")
public class RentACarService {
	@Context
	ServletContext ctx;
	
	public RentACarService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("rentACarDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("rentACarDAO", new RentACarDAO(contextPath));
		}
		
		if (ctx.getAttribute("locationDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("locationDAO", new LocationDAO(contextPath));
		}
		
		if (ctx.getAttribute("carDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("carDAO", new CarDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllObjects() {
		RentACarDAO dao = (RentACarDAO) ctx.getAttribute("rentACarDAO");
		Collection<RentACarDTO> allObjects = dao.getAll();
		
		return Response.status(200).entity(allObjects).build();
	}
	
	@GET
	@Path("/all-non-deleted")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllNonDeletedObjects() {
		RentACarDAO dao = (RentACarDAO) ctx.getAttribute("rentACarDAO");
		
		ArrayList<RentACarDTO> allObjects = new ArrayList<RentACarDTO>();
		for (RentACarDTO o: dao.getAll()) {
			if (!o.isLogicallyDeleted()) {
				allObjects.add(o);
			}
		}
		
		Comparator<RentACarDTO> byStatusInReverseOrder = 
				Comparator.comparing(RentACarDTO::getStatus).reversed()
				.thenComparing(RentACarDTO::getId);
		allObjects.sort(byStatusInReverseOrder);
		
		return Response.status(200).entity(allObjects).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getObjectById(@PathParam("id") long id) {
		RentACarDAO dao = (RentACarDAO) ctx.getAttribute("rentACarDAO");
		RentACarDTO object =  dao.getObjectById(id);
		
		return Response.status(200).entity(object).build();
 
	}
	
	@POST
	@Path("create-new-renting-object")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createNewRentingObject(@Context HttpServletRequest request, 
			RentACarDTO newRentingObject) {
		
		UserDTO loggedUser = (UserDTO) request.getSession().getAttribute("loggedUser");
		if(loggedUser != null) {
			if(!loggedUser.getRole().equals(Role.ADMIN.toString())) {
				return Response.status(400).entity("You don't have access!").build();
			}
		}else {
			return Response.status(400).entity("You don't have access!").build();
		}
		if (!RentingObjectValidator.isNewRentingObjectDTOValid(newRentingObject)) {
			return Response.status(400).entity("Invalid data!").build();
		}
		
		RentACarDAO rentACarDAO = (RentACarDAO) ctx.getAttribute("rentACarDAO");
		
		RentACarDTO existingRentingObjectWithSameName = 
				rentACarDAO.findByName(newRentingObject.getName());
		if (existingRentingObjectWithSameName != null) {
			return Response.status(400)
					.entity("Given name is already taken!").build();
		}
		
		RentACarDTO createdRentingObject = 
				rentACarDAO.createNewRentingObject(newRentingObject);
		
		return Response.status(201).entity(createdRentingObject).build();
	}
	
	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchRentingObjects(@QueryParam("name") String name, 
			@QueryParam("carType") String carType, 
			@QueryParam("location") String location, 
			@QueryParam("grade") double grade) {
		RentACarDAO rentACarDAO = (RentACarDAO) ctx.getAttribute("rentACarDAO");
	    Collection<RentACarDTO> allRentingObjects = rentACarDAO.getAll();
	    CarDAO carDAO = (CarDAO) ctx.getAttribute("carDAO");
	    LocationDAO locationDAO = (LocationDAO) ctx.getAttribute("locationDAO");
	    
	    ArrayList<Long> idsOfCarsThatMatchCarTypeParam = new ArrayList<Long>();
	    if (carType != null) {
	    	for (CarDTO carDTO: carDAO.getAll()) {
	    		if (carDTO.getType().toLowerCase().startsWith(carType.toLowerCase())) {
	    			idsOfCarsThatMatchCarTypeParam.add(carDTO.getId());
	    		}
	    	}
	    }
	    ArrayList<Long> idsOfLocationsThatMatchLocationParam = new ArrayList<Long>();
	    if (location != null) {
	    	for (Location l: locationDAO.getAll()) {
	    		if (l.getAddress().toLowerCase().contains(location.toLowerCase())) {
	    			idsOfLocationsThatMatchLocationParam.add(l.getId());
	    		}
	    	}
	    }
	    
	    if (name != null || carType != null || location != null || grade >= 1.0) {
	    	allRentingObjects = allRentingObjects.stream().filter(obj -> 
	    			(name == null || 
	    					obj.getName().toLowerCase().contains(name.toLowerCase())) && 
	    			(carType == null || doesRentingObjectContainACarOfSearchedType(
	    					obj, idsOfCarsThatMatchCarTypeParam)) && 
	    			(location == null || 
	    					doesLocationOfRentingObjectIncludeSearchedCityOrCountry(obj, 
	    							idsOfLocationsThatMatchLocationParam)) && 
	    			(grade < 1.0 || obj.getGrade() == grade)
	    	).collect(Collectors.toList());
	    }
	    
	    return Response.status(200).entity(allRentingObjects).build();
	}
	
	private boolean doesRentingObjectContainACarOfSearchedType(RentACarDTO rentingObject, 
			ArrayList<Long> idsOfCarsThatMatchCarTypeParam) {
		for (long carId: idsOfCarsThatMatchCarTypeParam) {
			for (long cId: rentingObject.getCars()) {
				if (cId == carId) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	private boolean doesLocationOfRentingObjectIncludeSearchedCityOrCountry(
			RentACarDTO rentingObject, 
			ArrayList<Long> idsOfLocationsThatMatchLocationParam) {
		for (long locationId: idsOfLocationsThatMatchLocationParam) {
			if (rentingObject.getLocationId() == locationId) {
				return true;
			}
		}
		
		return false;
	}
}
