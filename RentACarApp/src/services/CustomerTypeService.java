package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.CustomerType;
import beans.Location;
import dao.CustomerTypeDAO;
import dao.LocationDAO;

@Path("/customerTypes")
public class CustomerTypeService {
	@Context
	ServletContext ctx;
	
	public CustomerTypeService() {
		
	}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("customerTypeDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("customerTypeDAO", new CustomerTypeDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCustomerTypes() {
		CustomerTypeDAO dao = (CustomerTypeDAO) ctx.getAttribute("customerTypeDAO");
		Collection<CustomerType> allTypes = dao.getAll();
		
		return Response.status(200).entity(allTypes).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLocationById(@PathParam("id") long id) {
		CustomerTypeDAO dao = (CustomerTypeDAO) ctx.getAttribute("customerTypeDAO");
		CustomerType type =  dao.getLocationById(id);
		
		return Response.status(200).entity(type).build();
 
	}

}
