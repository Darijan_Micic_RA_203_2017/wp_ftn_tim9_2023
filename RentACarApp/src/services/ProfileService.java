package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.ProfileDAO;
import dao.UsersDAO;
import dtos.AllUsersDTO;
import dtos.UserDTO;
import validators.CommentValidator;
import validators.ProfileValidator;
import validators.UserValidator;

@Path("profiles")
public class ProfileService {
	@Context
	ServletContext ctx;
	
	public ProfileService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("profileDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("profileDAO", new ProfileDAO(contextPath));
		}
		
		if (ctx.getAttribute("usersDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("usersDAO", new UsersDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllProfiles() {
		ProfileDAO dao = (ProfileDAO) ctx.getAttribute("profileDAO");
		Collection<UserDTO> allUsers = dao.getAll();
		
		return Response.status(200).entity(allUsers).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserById(@Context HttpServletRequest request,@PathParam("id") long id) {
		UserDTO loggedUser = (UserDTO) request.getSession().getAttribute("loggedUser");
		if(loggedUser == null) {
			return Response.status(400).entity("You don't have access!").build();
		}
		
		ProfileDAO dao = (ProfileDAO) ctx.getAttribute("profileDAO");
		UserDTO user = dao.getById(id);
		
		return Response.status(200).entity(user).build();
	}
	
	@PUT
	@Path("/edit")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateUser(AllUsersDTO user) {
		if (!ProfileValidator.isUserDTOValid(user)) {
			return Response.status(400).entity("Invalid data!").build();
		}
		
		UsersDAO dao = (UsersDAO) ctx.getAttribute("usersDAO");
		AllUsersDTO updatedUser = dao.updateUser(user);
		
		return Response.status(200).entity(updatedUser).build();
	}
}
