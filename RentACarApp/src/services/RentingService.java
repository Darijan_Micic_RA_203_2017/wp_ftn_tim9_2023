package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.RentACarDAO;
import dao.RentingDAO;
import dtos.RentACarDTO;
import dtos.RentingDTO;

@Path("/rentings")
public class RentingService {
	
	@Context
	ServletContext ctx;
	
	public RentingService() {
		
	}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("rentingDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("rentingDAO", new RentingDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRentings() {
		RentingDAO dao = (RentingDAO) ctx.getAttribute("rentingDAO");
		Collection<RentingDTO> allRentings =  dao.getAll();
		
		return Response.status(200).entity(allRentings).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRentingsByObjectId(@PathParam("id") long id) {
		RentingDAO dao = (RentingDAO) ctx.getAttribute("rentingDAO");
		Collection<RentingDTO> rentings =  dao.getRentingsFromObject(id);
		
		return Response.status(200).entity(rentings).build();
 
	}
	
	@GET
	@Path("/forCustomer/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRentingsByCustomerId(@PathParam("id") long id) {
		RentingDAO dao = (RentingDAO) ctx.getAttribute("rentingDAO");
		Collection<RentingDTO> rentings =  dao.getRentingsFromObjectByCustomerId(id);
		
		return Response.status(200).entity(rentings).build();
 
	}
}
