package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.CarDAO;
import dao.CommentDAO;
import dtos.CarDTO;
import dtos.CommentDTO;
import dtos.UserDTO;
import beans.CommentStatus;
import beans.Role;
import validators.CommentValidator;
import validators.LocationValidator;

@Path("/comments")
public class CommentService {

	@Context
	ServletContext ctx;
	
	public CommentService() {
		
	}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("commentDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("commentDAO", new CommentDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllComments() {
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		Collection<CommentDTO> allComments = dao.getAll();
		
		return Response.status(200).entity(allComments).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllAcceptedCommentsFromObject(@PathParam("id")long id) {
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		Collection<CommentDTO> allComments = dao.getAllAcceptedFromObject(id);
		
		return Response.status(200).entity(allComments).build();
	}
	
	@GET
	@Path("/all/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCommentsFromObject(@Context HttpServletRequest request,@PathParam("id")long id) {
		UserDTO loggedUser = (UserDTO) request.getSession().getAttribute("loggedUser");
		if(loggedUser != null) {
			if(!loggedUser.getRole().equals(Role.MANAGER.toString())) {
				return Response.status(400).entity("You don't have access!").build();
			}
		}else {
			return Response.status(400).entity("You don't have access!").build();
		}
		
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		Collection<CommentDTO> allComments = dao.getAllFromObject(id);
		
		return Response.status(200).entity(allComments).build();
	}
	
	@GET
	@Path("/acceptedComments")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAcceptedComments() {
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		Collection<CommentDTO> acceptedComments = dao.getAcceptedComments();
		
		return Response.status(200).entity(acceptedComments).build();
	}
	
	@GET
	@Path("/acceptedAndRejectedComments/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllAcceptedAndRejectedCommentsFromObject(@PathParam("id")long id) {
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		Collection<CommentDTO> acceptedComments = dao.getAcceptedAndRejectedComments(id);
		
		return Response.status(200).entity(acceptedComments).build();
	}
	
	@PUT
	@Path("/acceptOrReject/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response acceptOrRejectComment(@Context HttpServletRequest request, @PathParam("id")long id, String status) {
		
		UserDTO loggedUser = (UserDTO) request.getSession().getAttribute("loggedUser");
		if(loggedUser != null) {
			if(!loggedUser.getRole().equals(Role.MANAGER.toString())) {
				return Response.status(400).entity("You don't have access!").build();
			}
		}else {
			return Response.status(400).entity("You don't have access!").build();
		}
		
		if(!(status.equals(CommentStatus.ACCEPTED.toString()) || status.equals(CommentStatus.REJECTED.toString()))) {
			return Response.status(400).entity("Invalid data!").build();
		}
		
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		CommentDTO editedComment = dao.acceptOrRejectComment(id, status);
		
		return Response.status(200).entity(editedComment).build();
	}
	
	@PUT
	@Path("/deleteComment/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteComment(@Context HttpServletRequest request,@PathParam("id")long id) {
		UserDTO loggedUser = (UserDTO) request.getSession().getAttribute("loggedUser");
		if(loggedUser != null) {
			if(!loggedUser.getRole().equals(Role.MANAGER.toString())) {
				return Response.status(400).entity("You don't have access!").build();
			}
		}else {
			return Response.status(400).entity("You don't have access!").build();
		}
		
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		CommentDTO deleted = dao.deleteComment(id);
		
		return Response.status(200).entity(deleted).build();
	}
	
	@POST
	@Path("/addComment")
	@Produces(MediaType.APPLICATION_JSON)
	public Response acceptOrRejectComment(CommentDTO comment) {
		if (!CommentValidator.isNewCommentValid(comment)) {
			return Response.status(400).entity("Invalid data!").build();
		}
		
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		CommentDTO addedComment = dao.addComment(comment);
		
		return Response.status(200).entity(addedComment).build();
	}
}
