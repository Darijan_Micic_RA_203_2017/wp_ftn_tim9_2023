package services;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Role;
import dao.RentACarDAO;
import dao.UsersDAO;
import dtos.AllUsersDTO;
import dtos.RentACarDTO;
import dtos.UserDTO;
import validators.CarValidator;
import validators.ProfileValidator;

@Path("/allUsers")
public class UsersService {
	@Context
	ServletContext ctx;
	
	public UsersService() {
		
	}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("usersDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("usersDAO", new UsersDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUsers(@Context HttpServletRequest request) {
		UserDTO loggedUser = (UserDTO) request.getSession().getAttribute("loggedUser");
		if(loggedUser != null) {
			if(!loggedUser.getRole().equals(Role.ADMIN.toString())) {
				return Response.status(400).entity("You don't have access!").build();
			}
		}else {
			return Response.status(400).entity("You don't have access!").build();
		}
		
		
		UsersDAO dao = (UsersDAO) ctx.getAttribute("usersDAO");
		Collection<AllUsersDTO> allUsers =  dao.getAll();
		
		return Response.status(200).entity(allUsers).build();
	}
	
	@GET
	@Path("/managers")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllManagers() {
		UsersDAO dao = (UsersDAO) ctx.getAttribute("usersDAO");
		Collection<AllUsersDTO> allManagers = dao.getAllManagers();
		
		return Response.status(200).entity(allManagers).build();
	}
	
	@GET
	@Path("/managers-who-do-not-have-a-renting-object")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllManagersWhoDoNotHaveARentingObject() {
		UsersDAO dao = (UsersDAO) ctx.getAttribute("usersDAO");
		Collection<AllUsersDTO> allManagersWhoDoNotHaveARentingObject = 
				dao.getAllManagersWhoDoNotHaveARentingObject();
		
		return Response.status(200).entity(allManagersWhoDoNotHaveARentingObject).build();
	}
	
	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchUsers(@QueryParam("firstName") String firstName, @QueryParam("lastName") String lastName,
	    @QueryParam("username") String username) {
	    UsersDAO dao = (UsersDAO) ctx.getAttribute("usersDAO");
	    Collection<AllUsersDTO> allUsers = dao.getAll();

	    if (firstName != null || lastName != null || username != null) {
	        allUsers = allUsers.stream()
	                .filter(user ->
	                    (firstName == null || user.getFirstName().toLowerCase().contains(firstName.toLowerCase())) &&
	                    (lastName == null || user.getLastName().toLowerCase().contains(lastName.toLowerCase())) &&
	                    (username == null || user.getUsername().toLowerCase().contains(username.toLowerCase()))
	                )
	                .collect(Collectors.toList());
	    }
	    return Response.status(200).entity(allUsers).build();
	}
	
	@GET
	@Path("/getCustomer/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerById(@PathParam("id") long id) {
		UsersDAO dao = (UsersDAO) ctx.getAttribute("usersDAO");
		AllUsersDTO customer =  dao.getCustomerById(id);
		
		return Response.status(200).entity(customer).build();
	}
	
	@GET
	@Path("/get-manager/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getManagerById(@PathParam("id") long id) {
		UsersDAO dao = (UsersDAO) ctx.getAttribute("usersDAO");
		AllUsersDTO manager =  dao.getManagerById(id);
		
		return Response.status(200).entity(manager).build();
	}
	
	@POST
	@Path("/registerCustomer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerCustomer(AllUsersDTO user) {
		UsersDAO dao = (UsersDAO) ctx.getAttribute("usersDAO");
		
		AllUsersDTO existingUsername = 
				dao.findByUsername(user.getUsername());
		if (existingUsername != null) {
			return Response.status(400)
					.entity("Given name is already taken!").build();
		}
		
		if (!ProfileValidator.isNewlyRegisteredCustomerDTOValid(user)) {
			return Response.status(400).entity("Invalid data!").build();
		}
		
		AllUsersDTO customer =  dao.addCustomer(user);
		
		return Response.status(200).entity(customer).build();
	}
	
	@POST
	@Path("/register-manager")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerNewManager(@Context HttpServletRequest request, AllUsersDTO user) {
		
		UserDTO loggedUser = (UserDTO) request.getSession().getAttribute("loggedUser");
		if(loggedUser != null) {
			if(!loggedUser.getRole().equals(Role.ADMIN.toString())) {
				return Response.status(400).entity("You don't have access!").build();
			}
		}else {
			return Response.status(400).entity("You don't have access!").build();
		}
		
		UsersDAO dao = (UsersDAO) ctx.getAttribute("usersDAO");
		
		AllUsersDTO existingUsername = 
				dao.findByUsername(user.getUsername());
		if (existingUsername != null) {
			return Response.status(400)
					.entity("Given name is already taken!").build();
		}
		
		AllUsersDTO manager =  dao.createNewManager(user);
		
		return Response.status(200).entity(manager).build();
	}
}
