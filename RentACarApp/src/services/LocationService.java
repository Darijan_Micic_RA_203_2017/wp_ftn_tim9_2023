package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Location;
import dao.LocationDAO;
import validators.LocationValidator;

@Path("/locations")
public class LocationService {
	@Context
	ServletContext ctx;
	
	public LocationService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("locationDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("locationDAO", new LocationDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllLocations() {
		LocationDAO dao = (LocationDAO) ctx.getAttribute("locationDAO");
		Collection<Location> allLocations = dao.getAll();
		
		return Response.status(200).entity(allLocations).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLocationById(@PathParam("id") long id) {
		LocationDAO dao = (LocationDAO) ctx.getAttribute("locationDAO");
		Location location =  dao.getLocationById(id);
		
		return Response.status(200).entity(location).build();
 
	}
	
	@POST
	@Path("create-new-location")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createNewLocation(@Context HttpServletRequest request, 
			Location newLocation) {
		if (!LocationValidator.isNewLocationValid(newLocation)) {
			return Response.status(400).entity("Invalid data!").build();
		}
		
		LocationDAO locationDAO = (LocationDAO) ctx.getAttribute("locationDAO");
		
		Location existingRentingObjectWithSameAddress = 
				locationDAO.findByAddress(newLocation.getAddress());
		if (existingRentingObjectWithSameAddress != null) {
			return Response.status(400)
					.entity("Given address is already taken!").build();
		}
		
		Location createdLocation = locationDAO.createNewLocation(newLocation);
		
		return Response.status(201).entity(createdLocation).build();
	}
}
