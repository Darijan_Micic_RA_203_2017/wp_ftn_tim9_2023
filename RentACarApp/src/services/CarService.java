package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Role;
import dao.CarDAO;
import dao.RentACarDAO;
import dtos.CarDTO;
import dtos.UserDTO;
import validators.CarValidator;

@Path("/cars")
public class CarService {
	@Context
	ServletContext ctx;
	
	public CarService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("carDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("carDAO", new CarDAO(contextPath));
		}
		
		if (ctx.getAttribute("rentACarDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("rentACarDAO", new RentACarDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCars() {
		CarDAO dao = (CarDAO) ctx.getAttribute("carDAO");
		Collection<CarDTO> allCars = dao.getAll();
		
		return Response.status(200).entity(allCars).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCarById(@PathParam("id") long id) {
		CarDAO dao = (CarDAO) ctx.getAttribute("carDAO");
		CarDTO car =  dao.getCarById(id);
		
		return Response.status(200).entity(car).build();
	}
	
	@POST
	@Path("create-new-car")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createNewCar(@Context HttpServletRequest request, CarDTO newCar) {
		UserDTO loggedUser = (UserDTO) request.getSession().getAttribute("loggedUser");
		if(loggedUser != null) {
			if(!loggedUser.getRole().equals(Role.MANAGER.toString())) {
				return Response.status(400).entity("You don't have access!").build();
			}
		}else {
			return Response.status(400).entity("You don't have access!").build();
		}
		
		if (!CarValidator.isNewCarDTOValid(newCar)) {
			return Response.status(400).entity("Invalid data!").build();
		}
		
		CarDAO carDAO = (CarDAO) ctx.getAttribute("carDAO");
		RentACarDAO rentACarDAO = (RentACarDAO) ctx.getAttribute("rentACarDAO");
		
		CarDTO createdCar = carDAO.createNewCar(newCar);
		rentACarDAO.addCarToRentingObject(createdCar);
		
		return Response.status(201).entity(createdCar).build();
	}
	
	@PUT
	@Path("/editCar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response editCar(@Context HttpServletRequest request, CarDTO car) {
		UserDTO loggedUser = (UserDTO) request.getSession().getAttribute("loggedUser");
		if(loggedUser != null) {
			if(!loggedUser.getRole().equals(Role.MANAGER.toString())) {
				return Response.status(400).entity("You don't have access!").build();
			}
		}else {
			return Response.status(400).entity("You don't have access!").build();
		}
		
		if (!CarValidator.isCarDTOValid(car)) {
			return Response.status(400).entity("Invalid data!").build();
		}
		
		CarDAO dao = (CarDAO) ctx.getAttribute("carDAO");
		CarDTO editedCar =  dao.editCar(car);
		
		return Response.status(200).entity(editedCar).build();
	}
	
	@PUT
	@Path("/deleteCar/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response logicallyDeleteCar(@Context HttpServletRequest request,@PathParam("id") long id) {
		UserDTO loggedUser = (UserDTO) request.getSession().getAttribute("loggedUser");
		if(loggedUser != null) {
			if(!loggedUser.getRole().equals(Role.MANAGER.toString())) {
				return Response.status(400).entity("You don't have access!").build();
			}
		}else {
			return Response.status(400).entity("You don't have access!").build();
		}
		
		CarDAO dao = (CarDAO) ctx.getAttribute("carDAO");
		CarDTO car =  dao.logicallyDeleteCar(id);
		
		return Response.status(200).entity(car).build();
	}
}
