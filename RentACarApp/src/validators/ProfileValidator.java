package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import beans.Gender;
import beans.Role;
import dtos.AllUsersDTO;
import dtos.UserDTO;

public class ProfileValidator {
	
	public ProfileValidator() {}
	
	public static boolean isNewlyRegisteredCustomerDTOValid(
			AllUsersDTO newlyRegisteredCustomerDTO) {
		boolean newlyRegisteredCustomerDTOValidity = true;
		
		if (!isUsernameValid(newlyRegisteredCustomerDTO.getUsername())) {
			newlyRegisteredCustomerDTOValidity = false;
		}
		
		if (!isPasswordValid(newlyRegisteredCustomerDTO.getPassword())) {
			newlyRegisteredCustomerDTOValidity = false;
		}
		
		if (!isNameValid(newlyRegisteredCustomerDTO.getFirstName())) {
			newlyRegisteredCustomerDTOValidity = false;
		}
		
		if (!isNameValid(newlyRegisteredCustomerDTO.getLastName())) {
			newlyRegisteredCustomerDTOValidity = false;
		}
		
		if (!isGenderValid(newlyRegisteredCustomerDTO.getGender())) {
			newlyRegisteredCustomerDTOValidity = false;
		}
		
		if (newlyRegisteredCustomerDTO.getRole() != null) {
			if (!newlyRegisteredCustomerDTO.getRole().equals(Role.CUSTOMER.toString())) {
				newlyRegisteredCustomerDTOValidity = false;
			}
		} else {
			newlyRegisteredCustomerDTOValidity = false;
		}
		
		return newlyRegisteredCustomerDTOValidity;
	}
	
	public static boolean isUserDTOValid(UserDTO userDTO) {
		boolean userDTOValidity = true;
		
		if (userDTO.getId() <= 0) {
			userDTOValidity = false;
		}
		
		if (!isUsernameValid(userDTO.getUsername())) {
			userDTOValidity = false;
		}
		
		if (!isPasswordValid(userDTO.getPassword())) {
			userDTOValidity = false;
		}
		
		if (!isNameValid(userDTO.getFirstName())) {
			userDTOValidity = false;
		}
		
		if (!isNameValid(userDTO.getLastName())) {
			userDTOValidity = false;
		}
		
		if (!isGenderValid(userDTO.getGender())) {
			userDTOValidity = false;
		}
		
		if (!isRoleValid(userDTO.getRole())) {
			userDTOValidity = false;
		}
		
		return userDTOValidity;
	}
	
	private static boolean isUsernameValid(String username) {
		if (username == null) {
			return false;
		}
		
		Pattern pattern = Pattern.compile("[a-zA-Z][-_a-zA-Z0-9\\.]+");
		Matcher matcher = pattern.matcher(username);
		if (!matcher.matches()) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isPasswordValid(String password) {
		if (password == null) {
			return false;
		}
		
		if (password.length() < 5 || password.length() > 15) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isNameValid(String name) {
		if (name == null) {
			return false;
		}
		
		Pattern pattern = Pattern
				.compile("[A-Z\\p{L}][a-z\\p{L}]+([ -][A-Z\\p{L}][a-z\\p{L}]+)*");
		Matcher matcher = pattern.matcher(name);
		if (!matcher.matches()) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isGenderValid(String gender) {
		if (gender == null) {
			return false;
		}
		
		if (gender.equals(Gender.MALE.toString()) || 
				gender.equals(Gender.FEMALE.toString())) {
			return true;
		}
		
		return false;
	}
	
	private static boolean isRoleValid(String role) {
		if (role == null) {
			return false;
		}
		
		if (role.equals(Role.CUSTOMER.toString()) || 
				role.equals(Role.MANAGER.toString()) || 
				role.equals(Role.ADMIN.toString())) {
			return true;
		}
		
		return false;
	}

}
