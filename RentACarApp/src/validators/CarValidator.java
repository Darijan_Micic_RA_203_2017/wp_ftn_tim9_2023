package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import beans.CarKind;
import beans.CarStatus;
import beans.CarType;
import beans.FuelType;
import dtos.CarDTO;

public class CarValidator {
	public CarValidator() {}
	
	public static boolean isNewCarDTOValid(CarDTO newCarDTO) {
		boolean newCarDTOValidity = true;
		
		if (newCarDTO.getId() != 0) {
			newCarDTOValidity = false;
		}
		
		if (newCarDTO.isLogicallyDeleted()) {
			newCarDTOValidity = false;
		}
		
		if (!isBrandValid(newCarDTO.getBrand())) {
			newCarDTOValidity = false;
		}
		
		if (!isPriceValid(newCarDTO.getPrice())) {
			newCarDTOValidity = false;
		}
		
		if (!isTypeValid(newCarDTO.getType())) {
			newCarDTOValidity = false;
		}
		
		if (newCarDTO.getRentACarObjectId() <= 0) {
			newCarDTOValidity = false;
		}
		
		if (!isKindValid(newCarDTO.getKind())) {
			newCarDTOValidity = false;
		}
		
		if (!isFuelValid(newCarDTO.getFuel())) {
			newCarDTOValidity = false;
		}
		
		if (!isConsumptionValid(newCarDTO.getConsumption())) {
			newCarDTOValidity = false;
		}
		
		if (!isNumberOfDoorsValid(newCarDTO.getNoDoors())) {
			newCarDTOValidity = false;
		}
		
		if (!isNumberOfPeopleValid(newCarDTO.getNoPeople())) {
			newCarDTOValidity = false;
		}
		
		if (!isDescriptionValid(newCarDTO.getDescription())) {
			newCarDTOValidity = false;
		}
		
		if (!isImageValid(newCarDTO.getImage())) {
			newCarDTOValidity = false;
		}
		
		if (newCarDTO.getStatus() != null) {
			if (!newCarDTO.getStatus().equals(CarStatus.FREE.toString())) {
				newCarDTOValidity = false;
			}
		} else {
			newCarDTOValidity = false;
		}
		
		return newCarDTOValidity;
	}
	
	public static boolean isCarDTOValid(CarDTO carDTO) {
		boolean carDTOValidity = true;
		
		if (carDTO.getId() <= 0) {
			carDTOValidity = false;
		}
		
		if (!isBrandValid(carDTO.getBrand())) {
			carDTOValidity = false;
		}
		
		if (!isPriceValid(carDTO.getPrice())) {
			carDTOValidity = false;
		}
		
		if (!isTypeValid(carDTO.getType())) {
			carDTOValidity = false;
		}
		
		if (carDTO.getRentACarObjectId() <= 0) {
			carDTOValidity = false;
		}
		
		if (!isKindValid(carDTO.getKind())) {
			carDTOValidity = false;
		}
		
		if (!isFuelValid(carDTO.getFuel())) {
			carDTOValidity = false;
		}
		
		if (!isConsumptionValid(carDTO.getConsumption())) {
			carDTOValidity = false;
		}
		
		if (!isNumberOfDoorsValid(carDTO.getNoDoors())) {
			carDTOValidity = false;
		}
		
		if (!isNumberOfPeopleValid(carDTO.getNoPeople())) {
			carDTOValidity = false;
		}
		
		if (!isDescriptionValid(carDTO.getDescription())) {
			carDTOValidity = false;
		}
		
		if (!isImageValid(carDTO.getImage())) {
			carDTOValidity = false;
		}
		
		if (!isStatusValid(carDTO.getStatus())) {
			carDTOValidity = false;
		}
		
		return carDTOValidity;
	}
	
	private static boolean isBrandValid(String brand) {
		if (brand == null) {
			return false;
		}
		
		Pattern pattern = Pattern.compile("[A-Z\\p{L}][A-Za-z\\p{L}0-9 -\\.]+");
		Matcher matcher = pattern.matcher(brand);
		if (!matcher.matches()) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isPriceValid(double price) {
		if (price < 0) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isTypeValid(String type) {
		if (type == null) {
			return false;
		}
		
		if (type.equals(CarType.AUTO.toString()) || 
				type.equals(CarType.COMBI.toString()) || 
				type.equals(CarType.MOBILEHOME.toString())) {
			return true;
		}
		
		return true;
	}
	
	private static boolean isKindValid(String kind) {
		if (kind == null) {
			return false;
		}
		
		if (kind.equals(CarKind.MANUEL.toString()) || 
				kind.equals(CarKind.AUTOMATIC.toString())) {
			return true;
		}
		
		return true;
	}
	
	private static boolean isFuelValid(String fuel) {
		if (fuel == null) {
			return false;
		}
		
		if (fuel.equals(FuelType.BENZIN.toString()) || 
				fuel.equals(FuelType.DISEL.toString()) || 
				fuel.equals(FuelType.HYBRID.toString()) || 
				fuel.equals(FuelType.ELECTRIC.toString())) {
			return true;
		}
		
		return true;
	}
	
	private static boolean isConsumptionValid(int consumption) {
		if (consumption < 1) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isNumberOfDoorsValid(int noDoors) {
		if (noDoors < 2 || noDoors > 7) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isNumberOfPeopleValid(int noPeople) {
		if (noPeople < 1) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isDescriptionValid(String description) {
		if (description == null) {
			return false;
		}
		
		if (description.equals("")) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isImageValid(String image) {
		if (image == null) {
			return false;
		}
		
		if (image.equals("")) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isStatusValid(String status) {
		if (status == null) {
			return false;
		}
		
		if (status.equals(CarStatus.FREE.toString()) || 
				status.equals(CarStatus.BOOKED.toString())) {
			return true;
		}
		
		return false;
	}
}
