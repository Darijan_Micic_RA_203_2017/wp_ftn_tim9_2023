package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import beans.RentACarStatus;
import dtos.RentACarDTO;

public class RentingObjectValidator {
	public RentingObjectValidator() {}
	
	public static boolean isNewRentingObjectDTOValid(RentACarDTO newRentingObjectDTO) {
		boolean newRentingObjectDTOValidity = true;
		
		if (newRentingObjectDTO.getId() != 0) {
			newRentingObjectDTOValidity = false;
		}
		
		if (newRentingObjectDTO.isLogicallyDeleted()) {
			newRentingObjectDTOValidity = false;
		}
		
		if (!isNameValid(newRentingObjectDTO.getName())) {
			newRentingObjectDTOValidity = false;
		}
		
		if (!isHourValid(newRentingObjectDTO.getStartHour())) {
			newRentingObjectDTOValidity = false;
		}
		
		if (!isMinuteValid(newRentingObjectDTO.getStartMinute())) {
			newRentingObjectDTOValidity = false;
		}
		
		if (!isHourValid(newRentingObjectDTO.getEndHour())) {
			newRentingObjectDTOValidity = false;
		}
		
		if (!isMinuteValid(newRentingObjectDTO.getEndMinute())) {
			newRentingObjectDTOValidity = false;
		}
		
		if (newRentingObjectDTO.getStatus() != null) {
			if (!newRentingObjectDTO.getStatus().equals(RentACarStatus.NOTWORKING.toString())) {
				newRentingObjectDTOValidity = false;
			}
		} else {
			newRentingObjectDTOValidity = false;
		}
		
		if (newRentingObjectDTO.getLocationId() <= 0) {
			newRentingObjectDTOValidity = false;
		}
		
		if (!isImageValid(newRentingObjectDTO.getImage())) {
			newRentingObjectDTOValidity = false;
		}
		
		if (newRentingObjectDTO.getGrade() != 0.0) {
			newRentingObjectDTOValidity = false;
		}
		
		if (newRentingObjectDTO.getCars() != null) {
			if (!newRentingObjectDTO.getCars().isEmpty()) {
				newRentingObjectDTOValidity = false;
			}
		} else {
			newRentingObjectDTOValidity = false;
		}
		
		return newRentingObjectDTOValidity;
	}
	
	public static boolean isRentingObjectDTOValid(RentACarDTO rentingObjectDTO) {
		boolean rentingObjectDTOValidity = true;
		
		if (rentingObjectDTO.getId() <= 0) {
			rentingObjectDTOValidity = false;
		}
		
		if (!isNameValid(rentingObjectDTO.getName())) {
			rentingObjectDTOValidity = false;
		}
		
		if (!isHourValid(rentingObjectDTO.getStartHour())) {
			rentingObjectDTOValidity = false;
		}
		
		if (!isMinuteValid(rentingObjectDTO.getStartMinute())) {
			rentingObjectDTOValidity = false;
		}
		
		if (!isHourValid(rentingObjectDTO.getEndHour())) {
			rentingObjectDTOValidity = false;
		}
		
		if (!isMinuteValid(rentingObjectDTO.getEndMinute())) {
			rentingObjectDTOValidity = false;
		}
		
		if (!isStatusValid(rentingObjectDTO.getStatus())) {
			rentingObjectDTOValidity = false;
		}
		
		if (rentingObjectDTO.getLocationId() <= 0) {
			rentingObjectDTOValidity = false;
		}
		
		if (!isImageValid(rentingObjectDTO.getImage())) {
			rentingObjectDTOValidity = false;
		}
		
		if (rentingObjectDTO.getGrade() < 0.0 || rentingObjectDTO.getGrade() > 5.0) {
			rentingObjectDTOValidity = false;
		}
		
		if (rentingObjectDTO.getCars() == null) {
			rentingObjectDTOValidity = false;
		}
		
		return rentingObjectDTOValidity;
	}
	
	private static boolean isNameValid(String name) {
		if (name == null) {
			return false;
		}
		
		Pattern pattern = Pattern.compile("[A-Z\\p{L}][A-Za-z\\p{L}0-9 -\\.]+");
		Matcher matcher = pattern.matcher(name);
		if (!matcher.matches()) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isHourValid(int hour) {
		if (hour < 0 || hour > 23) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isMinuteValid(int minute) {
		if (minute < 0 || minute > 59) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isStatusValid(String status) {
		if (status == null) {
			return false;
		}
		
		if (status.equals(RentACarStatus.WORKING.toString()) || 
				status.equals(RentACarStatus.NOTWORKING.toString())) {
			return true;
		}
		
		return false;
	}
	
	private static boolean isImageValid(String image) {
		if (image == null) {
			return false;
		}
		
		if (image.equals("")) {
			return false;
		}
		
		return true;
	}
}
