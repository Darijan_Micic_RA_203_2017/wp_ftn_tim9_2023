package validators;

import dtos.CommentDTO;

public class CommentValidator {
	public CommentValidator() {}
	
	public static boolean isNewCommentValid(CommentDTO comment) {
		boolean newCommentValidity = true;
		
		if (comment.getUserId() == 0) {
			newCommentValidity = false;
		}
		
		if(comment.getRentACarObjectId() == 0){
			newCommentValidity = false;
		}
		
		if(comment.getRating() == null) {
			newCommentValidity = false;
		}
		
		if(comment.getRating() > 5) {
			newCommentValidity = false;
		}
		
		if(comment.getRating() < 1) {
			newCommentValidity = false;
		}
		
		if(comment.getStatus() == null) {
			newCommentValidity = false;
		}
		
		if(comment.getComment() == null) {
			newCommentValidity = false;
		}
		
		return newCommentValidity;
	}
}
