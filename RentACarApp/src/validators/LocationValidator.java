package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import beans.Location;

public class LocationValidator {
	public LocationValidator() {}
	
	public static boolean isNewLocationValid(Location newLocation) {
		boolean newLocationValidity = true;
		
		if (newLocation.getId() != 0) {
			newLocationValidity = false;
		}
		
		if (newLocation.isLogicallyDeleted()) {
			newLocationValidity = false;
		}
		
		if (!isLongitudeValid(newLocation.getLongitude())) {
			newLocationValidity = false;
		}
		
		if (!isLatitudeValid(newLocation.getLatitude())) {
			newLocationValidity = false;
		}
		
		if (!isAddressValid(newLocation.getAddress())) {
			newLocationValidity = false;
		}
		
		return newLocationValidity;
	}
	
	public static boolean isLocationValid(Location location) {
		boolean locationValidity = true;
		
		if (location.getId() <= 0) {
			locationValidity = false;
		}
		
		if (!isLongitudeValid(location.getLongitude())) {
			locationValidity = false;
		}
		
		if (!isLatitudeValid(location.getLatitude())) {
			locationValidity = false;
		}
		
		if (!isAddressValid(location.getAddress())) {
			locationValidity = false;
		}
		
		return locationValidity;
	}
	
	private static boolean isLongitudeValid(double longitude) {
		if (longitude < -180.0 || longitude > 180.0) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isLatitudeValid(double latitude) {
		if (latitude < -90.0 || latitude > 90.0) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isAddressValid(String address) {
		if (address == null) {
			return false;
		}
		
		Pattern pattern = Pattern.compile("[A-Z\\p{L}][A-Za-z\\p{L}0-9 -\\.]+");
		Matcher matcher = pattern.matcher(address);
		if (!matcher.matches()) {
			return false;
		}
		
		return true;
	}
}
