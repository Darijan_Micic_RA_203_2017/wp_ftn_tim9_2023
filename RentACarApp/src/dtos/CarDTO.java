package dtos;

public class CarDTO {
	private long id;
	private boolean logicallyDeleted;
	private String brand;
	private double price;
	private String type;
	private long rentACarObjectId;
	private String kind;
	private String fuel;
	private int consumption;
	private int noDoors;
	private int noPeople;
	private String description;
	private String image;
	private String status;
	
	public CarDTO() {}
	
	public CarDTO(long id, boolean logicallyDeleted, String brand, double price, 
			String type, long rentACarObjectId, String kind, String fuel, 
			int consumption, int noDoors, int noPeople, String description, String image, 
			String status) {
		this.id = id;
		this.logicallyDeleted = logicallyDeleted;
		this.brand = brand;
		this.price = price;
		this.type = type;
		this.rentACarObjectId = rentACarObjectId;
		this.kind = kind;
		this.fuel = fuel;
		this.consumption = consumption;
		this.noDoors = noDoors;
		this.noPeople = noPeople;
		this.description = description;
		this.image = image;
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public boolean isLogicallyDeleted() {
		return logicallyDeleted;
	}

	public void setLogicallyDeleted(boolean logicallyDeleted) {
		this.logicallyDeleted = logicallyDeleted;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getRentACarObjectId() {
		return rentACarObjectId;
	}

	public void setRentACarObjectId(long rentACarObjectId) {
		this.rentACarObjectId = rentACarObjectId;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getFuel() {
		return fuel;
	}

	public void setFuel(String fuel) {
		this.fuel = fuel;
	}

	public int getConsumption() {
		return consumption;
	}

	public void setConsumption(int consumption) {
		this.consumption = consumption;
	}

	public int getNoDoors() {
		return noDoors;
	}

	public void setNoDoors(int noDoors) {
		this.noDoors = noDoors;
	}

	public int getNoPeople() {
		return noPeople;
	}

	public void setNoPeople(int noPeople) {
		this.noPeople = noPeople;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append(id).append(";").append(logicallyDeleted).append(";").append(brand)
				.append(";").append(price).append(";").append(type).append(";")
				.append(rentACarObjectId).append(";").append(kind).append(";")
				.append(fuel).append(";").append(consumption).append(";").append(noDoors)
				.append(";").append(noPeople).append(";").append(description).append(";")
				.append(image).append(";").append(status);
		
		return builder.toString();
	}
}
