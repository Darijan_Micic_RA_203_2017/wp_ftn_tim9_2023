package dtos;

import java.util.List;

import beans.Car;
import beans.Location;
import beans.RentACarStatus;

public class RentACarDTO {
	private long id;
	private boolean logicallyDeleted;
	private String name;
	private Integer startHour;
	private Integer startMinute;
	private Integer endHour;
	private Integer endMinute;
	private String status;
	private long locationId;
	private String image;
	private double grade;
	private List<Long> cars;
	
	public RentACarDTO() {}
	
	public RentACarDTO(long id, boolean logicallyDeleted, String name, List<Long> cars, 
			Integer startHour, Integer startMinute, Integer endHour, Integer endMinute, 
			String status, long locationId, String image, double grade) {
		this.id = id;
		this.logicallyDeleted = logicallyDeleted;
		this.name = name;
		this.cars = cars;
		this.startHour = startHour;
		this.startMinute = startMinute;
		this.endHour = endHour;
		this.endMinute = endMinute;
		this.status = status;
		this.locationId = locationId;
		this.image = image;
		this.grade = grade;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public boolean isLogicallyDeleted() {
		return logicallyDeleted;
	}
	
	public void setLogicallyDeleted(boolean logicallyDeleted) {
		this.logicallyDeleted = logicallyDeleted;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStartHour() {
		return startHour;
	}

	public void setStartHour(Integer startHour) {
		this.startHour = startHour;
	}

	public Integer getStartMinute() {
		return startMinute;
	}

	public void setStartMinute(Integer startMinute) {
		this.startMinute = startMinute;
	}

	public Integer getEndHour() {
		return endHour;
	}

	public void setEndHour(Integer endHour) {
		this.endHour = endHour;
	}

	public Integer getEndMinute() {
		return endMinute;
	}

	public void setEndMinute(Integer endMinute) {
		this.endMinute = endMinute;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public double getGrade() {
		return grade;
	}

	public void setGrade(double grade) {
		this.grade = grade;
	}

	
	public List<Long> getCars() {
		return cars;
	}

	public void setCars(List<Long> cars) {
		this.cars = cars;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append(id).append(";").append(logicallyDeleted).append(";").append(name)
				.append(";").append(startHour).append(";").append(startMinute).append(";")
				.append(endHour).append(";").append(endMinute).append(";").append(status)
				.append(";").append(locationId).append(";").append(image).append(";")
				.append(grade).append(";");
		for (int i = 0; i < cars.size(); i++) {
			builder.append(cars.get(i));
			if (i < cars.size() - 1) {
				builder.append("|");
			}
		}
		
		return builder.toString();
	}
}
