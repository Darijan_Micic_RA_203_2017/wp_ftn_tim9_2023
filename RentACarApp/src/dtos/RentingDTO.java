package dtos;

import java.util.Date;
import java.util.List;

import beans.Car;
import beans.Customer;
import beans.RentACar;
import beans.RentingStatus;

public class RentingDTO {
	private String id;
	private List<Long> carsIds;
	private long rentACarObjectId;
	private String date;
	private Integer length;
	private double price;
	private long customerId;
	private String status;
	
	public RentingDTO() {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Long> getCarsIds() {
		return carsIds;
	}

	public void setCarsIds(List<Long> carsIds) {
		this.carsIds = carsIds;
	}

	public long getRentACarObjectId() {
		return rentACarObjectId;
	}

	public void setRentACarObjectId(long rentACarObjectId) {
		this.rentACarObjectId = rentACarObjectId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public RentingDTO(String id, List<Long> carsIds, long rentACarObjectId, String date, Integer length, double price,
			long customerId, String status) {
		super();
		this.id = id;
		this.carsIds = carsIds;
		this.rentACarObjectId = rentACarObjectId;
		this.date = date;
		this.length = length;
		this.price = price;
		this.customerId = customerId;
		this.status = status;
	}
	
	
}
