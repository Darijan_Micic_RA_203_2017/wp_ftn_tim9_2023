package dtos;

import beans.CustomerType;

public class AllUsersDTO extends UserDTO {

	private Integer collectedPoints;
	private long customerTypeId;
	private long rentACarObjectId;
	
	public AllUsersDTO() {}
	
	

	public AllUsersDTO(long id, boolean logicallyDeleted, String username, String password, String firstName,
			String lastName, String gender, String dateOfBirth, String role, Integer collectedPoints, long customerTypeId,
			long rentACarObjectId) {
		super(id, logicallyDeleted, username, password, firstName, lastName, gender, dateOfBirth, role);
		this.collectedPoints = collectedPoints;
		this.customerTypeId = customerTypeId;
		this.rentACarObjectId = rentACarObjectId;
	}



	public Integer getCollectedPoints() {
		return collectedPoints;
	}

	public void setCollectedPoints(Integer collectedPoints) {
		this.collectedPoints = collectedPoints;
	}

	public long getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(long customerTypeId) {
		this.customerTypeId = customerTypeId;
	}



	public long getRentACarObjectId() {
		return rentACarObjectId;
	}



	public void setRentACarObjectId(long rentACarObjectId) {
		this.rentACarObjectId = rentACarObjectId;
	}
	
	
}
