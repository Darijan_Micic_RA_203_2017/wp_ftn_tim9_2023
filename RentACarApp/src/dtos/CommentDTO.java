package dtos;

import beans.CommentStatus;
import beans.RentACar;
import beans.User;

public class CommentDTO {
	private long id;
	private boolean isLogicallyDeleted;
	private long userId;
	private long rentACarObjectId;
	private String comment;
	private Integer rating;
	private String status;
	
	public CommentDTO() {}

	

	public CommentDTO(long id, boolean isLogicallyDeleted, long userId, long rentACarObjectId, String comment,
			Integer rating, String status) {
		super();
		this.id = id;
		this.isLogicallyDeleted = isLogicallyDeleted;
		this.userId = userId;
		this.rentACarObjectId = rentACarObjectId;
		this.comment = comment;
		this.rating = rating;
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}
	
	public boolean isLogicallyDeleted() {
		return isLogicallyDeleted;
	}

	public void setLogicallyDeleted(boolean isLogicallyDeleted) {
		this.isLogicallyDeleted = isLogicallyDeleted;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getRentACarObjectId() {
		return rentACarObjectId;
	}

	public void setRentACarObjectId(long rentACarObjectId) {
		this.rentACarObjectId = rentACarObjectId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	
	
}
