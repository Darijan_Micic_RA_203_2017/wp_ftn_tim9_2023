package beans;

public class Comment {
	
	private long id;
	private User user;
	private RentACar rentACarObject;
	private String comment;
	private Integer rating;
	private CommentStatus status;
	
	public Comment() {}
	

	public Comment(long id, User user, RentACar rentACarObject, String comment, Integer rating, CommentStatus status) {
		super();
		this.id = id;
		this.user = user;
		this.rentACarObject = rentACarObject;
		this.comment = comment;
		this.rating = rating;
		this.status = status;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public RentACar getRentACarObject() {
		return rentACarObject;
	}

	public void setRentACarObject(RentACar rentACarObject) {
		this.rentACarObject = rentACarObject;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}


	public CommentStatus getStatus() {
		return status;
	}


	public void setStatus(CommentStatus status) {
		this.status = status;
	}

	
	
	
}
