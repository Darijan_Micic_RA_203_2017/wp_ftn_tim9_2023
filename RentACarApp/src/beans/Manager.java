package beans;

import java.util.Date;

public class Manager extends User{
	private RentACar rentACarObject;
	
	public Manager() {}
	

	public Manager(long id,boolean logicallyDeleted, String username, String password, String firstName, 
			String lastName, Gender gender, Date dateOfBirth, Role role,RentACar rentACarObject) {
		super(id,logicallyDeleted, username, password, firstName, lastName, gender, dateOfBirth, role);
		this.rentACarObject = rentACarObject;
	}


	public RentACar getRentACarObject() {
		return rentACarObject;
	}

	public void setRentACarObject(RentACar rentACarObject) {
		this.rentACarObject = rentACarObject;
	}
	

}
