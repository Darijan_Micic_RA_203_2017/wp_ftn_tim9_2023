package beans;

public class Location {
	private long id;
	private boolean logicallyDeleted;
	private double longitude;
	private double latitude;
	private String address;
	
	public Location() {}
	
	public Location(long id, boolean logicallyDeleted, double longitude, double latitude, 
			String address) {
		this.id = id;
		this.logicallyDeleted = logicallyDeleted;
		this.longitude = longitude;
		this.latitude = latitude;
		this.address = address;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isLogicallyDeleted() {
		return logicallyDeleted;
	}

	public void setLogicallyDeleted(boolean logicallyDeleted) {
		this.logicallyDeleted = logicallyDeleted;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append(id).append(";").append(logicallyDeleted).append(";")
				.append(longitude).append(";").append(latitude).append(";")
				.append(address);
		
		return builder.toString();
	}
}
