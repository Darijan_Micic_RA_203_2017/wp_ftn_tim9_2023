package beans;

public enum RentingStatus {
	PENDING, ACCEPTED, DOWNLOADED, RETURNED, REJECTED, CANCELLED
}
