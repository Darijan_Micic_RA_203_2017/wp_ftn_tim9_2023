package beans;

import java.util.List;

public class ShoppingCart {
	
	private long id;
	private String number;
	private List<Car> cars;
	private User user;
	private double price;
	
	public ShoppingCart() {}

	public ShoppingCart(long id, String number, List<Car> cars, User user, double price) {
		super();
		this.id = id;
		this.number = number;
		this.cars = cars;
		this.user = user;
		this.price = price;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
}
