package beans;

public enum CommentStatus {
	PENDING, ACCEPTED, REJECTED
}
