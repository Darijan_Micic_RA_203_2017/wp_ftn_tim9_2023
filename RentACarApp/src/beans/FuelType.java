package beans;

public enum FuelType {
	DISEL, BENZIN, HYBRID, ELECTRIC
}
