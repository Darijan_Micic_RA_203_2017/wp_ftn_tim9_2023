package beans;

import java.util.Date;
import java.util.List;

public class Customer extends User{
	private List<Renting> renting;
	private ShoppingCart cart;
	private Integer collectedPoints;
	private CustomerType type;
	
	public Customer() {}

	public Customer(long id,boolean logicallyDeleted, String username, String password, String firstName, 
			String lastName, Gender gender, Date dateOfBirth, Role role,
			List<Renting> renting, ShoppingCart cart, Integer collectedPoints, CustomerType type) {
		super(id,logicallyDeleted, username, password, firstName, lastName, gender, dateOfBirth, role);
		this.renting = renting;
		this.cart = cart;
		this.collectedPoints = collectedPoints;
		this.type = type;
	}

	public List<Renting> getRenting() {
		return renting;
	}

	public void setRenting(List<Renting> renting) {
		this.renting = renting;
	}

	public ShoppingCart getCart() {
		return cart;
	}

	public void setCart(ShoppingCart cart) {
		this.cart = cart;
	}

	public Integer getCollectedPoints() {
		return collectedPoints;
	}

	public void setCollectedPoints(Integer collectedPoints) {
		this.collectedPoints = collectedPoints;
	}

	public CustomerType getType() {
		return type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}
	
	
	
}
