package beans;

public class Car {
	
	private long id;
	private String brand;
	private double price;
	private CarType type;
	private RentACar rentACarObject;
	private CarKind kind;
	private FuelType fuel;
	private Integer consumption;
	private Integer noDoors;
	private Integer noPeople;
	private String description;
	private String image;
	private CarStatus status;

	public Car() {}
	
	public Car(long id, String brand, double price, CarType type, RentACar rentACarObject, CarKind kind, FuelType fuel,
			Integer consumption, Integer noDoors, Integer noPeople, String description, String image,
			CarStatus status) {
		super();
		this.id = id;
		this.brand = brand;
		this.price = price;
		this.type = type;
		this.rentACarObject = rentACarObject;
		this.kind = kind;
		this.fuel = fuel;
		this.consumption = consumption;
		this.noDoors = noDoors;
		this.noPeople = noPeople;
		this.description = description;
		this.image = image;
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public CarType getType() {
		return type;
	}

	public void setType(CarType type) {
		this.type = type;
	}

	public RentACar getRentACarObject() {
		return rentACarObject;
	}

	public void setRentACarObject(RentACar rentACarObject) {
		this.rentACarObject = rentACarObject;
	}

	public CarKind getKind() {
		return kind;
	}

	public void setKind(CarKind kind) {
		this.kind = kind;
	}

	public FuelType getFuel() {
		return fuel;
	}

	public void setFuel(FuelType fuel) {
		this.fuel = fuel;
	}

	public Integer getConsumption() {
		return consumption;
	}

	public void setConsumption(Integer consumption) {
		this.consumption = consumption;
	}

	public Integer getNoDoors() {
		return noDoors;
	}

	public void setNoDoors(Integer noDoors) {
		this.noDoors = noDoors;
	}

	public Integer getNoPeople() {
		return noPeople;
	}

	public void setNoPeople(Integer noPeople) {
		this.noPeople = noPeople;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public CarStatus getStatus() {
		return status;
	}

	public void setStatus(CarStatus status) {
		this.status = status;
	}
	
	
}
