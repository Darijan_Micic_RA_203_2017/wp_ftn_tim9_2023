package beans;

import java.util.Date;
import java.util.List;

public class Renting {
	private String id;//10 karaktera
	private List<Car> cars;
	private RentACar rentACarObject;
	private Date date;
	private Integer length;
	private double price;
	private Customer customer;
	private RentingStatus status;
	
	public Renting() {}

	public Renting(String id, List<Car> cars, RentACar rentACarObject, Date date, Integer length, double price,
			Customer customer, RentingStatus status) {
		super();
		this.id = id;
		this.cars = cars;
		this.rentACarObject = rentACarObject;
		this.date = date;
		this.length = length;
		this.price = price;
		this.customer = customer;
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	public RentACar getRentACarObject() {
		return rentACarObject;
	}

	public void setRentACarObject(RentACar rentACarObject) {
		this.rentACarObject = rentACarObject;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public RentingStatus getStatus() {
		return status;
	}

	public void setStatus(RentingStatus status) {
		this.status = status;
	}
	
	
}
