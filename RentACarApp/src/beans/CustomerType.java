package beans;

public class CustomerType {
	private long id;
	private String typeName;
	private Integer discount;
	private Integer requiredPoints;
	
	public CustomerType() {}
	

	public CustomerType(long id, String typeName, Integer discount, Integer requiredPoints) {
		super();
		this.id = id;
		this.typeName = typeName;
		this.discount = discount;
		this.requiredPoints = requiredPoints;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public Integer getRequiredPoints() {
		return requiredPoints;
	}

	public void setRequiredPoints(Integer requiredPoints) {
		this.requiredPoints = requiredPoints;
	}
	
	
}
