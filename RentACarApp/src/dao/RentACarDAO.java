package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;

import dtos.CarDTO;
import dtos.RentACarDTO;

public class RentACarDAO {
	private Map<Long, RentACarDTO> objects = new HashMap<Long, RentACarDTO>();
	private String path = null;
	
	public RentACarDAO(String context){
		this.path = context;
		loadObjects(context);
	}
	
	public Collection<RentACarDTO> getAll(){
		return objects.values();
	}
	
	public RentACarDTO getObjectById(long id) {
		return objects.get(id);
	}
	
	public RentACarDTO findByName(String name) {
		for (RentACarDTO rObj: objects.values()) {
			if (rObj.getName().equals(name)) {
				return rObj;
			}
		}
		
		return null;
	}
	
	public RentACarDTO createNewRentingObject(RentACarDTO newRentingObject) {
		long idOfNewRentingObject = generateNewId();
		newRentingObject.setId(idOfNewRentingObject);
		objects.put(idOfNewRentingObject, newRentingObject);
		Save();
		
		return newRentingObject;
	}
	
	private long generateNewId() {
		long maxId = 0;
		for (long id: objects.keySet()) {
			if (id > maxId) {
				maxId = id;
			}
		}
		
		long newId = maxId + 1;
		
		return newId;
	}
	
	public void addCarToRentingObject(CarDTO createdCar) {
		RentACarDTO rentingObject = objects.get(createdCar.getRentACarObjectId());
		rentingObject.getCars().add(createdCar.getId());
		
		Save();
	}
	
	private void loadObjects(String context) {
		BufferedReader in = null;

		try {
			File file = new File(context + "/rentACarObjects.txt");
			in = new BufferedReader(new FileReader(file));
			String line;
			
			String id = "";
			String logicallyDeleted = "";
			String name = "";
			String startHour = "";
			String startMinute = "";
			String endHour = "";
			String endMinute = "";
			String status="";
			String locationId = "";
			String image = "";
			String grade = "";
			String cars = "";
			
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}
				
				List<Long> carIds = new ArrayList<Long>();
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					id = st.nextToken().trim();
					logicallyDeleted = st.nextToken().trim();
					name = st.nextToken().trim();
					startHour = st.nextToken().trim();
					startMinute = st.nextToken().trim();
					endHour = st.nextToken().trim();
					endMinute = st.nextToken().trim();
					status = st.nextToken().trim();
					locationId = st.nextToken().trim();
					image = st.nextToken().trim();
					grade = st.nextToken().trim();
					try {
						cars = st.nextToken().trim();
					} catch (Exception e) {
						cars = "";
					}
				}
				
				st = new StringTokenizer(cars, "|");
				while (st.hasMoreTokens()) {
					try {
						String carId = st.nextToken().trim();
						carIds.add(Long.parseLong(carId));
					} catch (Exception e) {
						carIds = new ArrayList<Long>();
					}
				}
				
				objects.put(Long.parseLong(id), new RentACarDTO(Long.parseLong(id), 
						Boolean.parseBoolean(logicallyDeleted), name, carIds, 
						Integer.parseInt(startHour), Integer.parseInt(startMinute), 
						Integer.parseInt(endHour), Integer.parseInt(endMinute), status, 
						Long.parseLong(locationId), image, Double.parseDouble(grade)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null ) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void Save() {
		BufferedWriter bw = null;
		try {	
			File file = new File(path + "/rentACarObjects.txt");
			System.out.println(file);
			
			FileOutputStream fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			for (RentACarDTO rDTO: objects.values()) {
				bw.write(rDTO.toString());
				bw.newLine();
			}
			
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
	}
}
