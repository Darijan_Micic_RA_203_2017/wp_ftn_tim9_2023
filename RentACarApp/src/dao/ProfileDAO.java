package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.StringTokenizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.io.File;
import java.io.FileOutputStream;

import beans.Gender;
import beans.Role;
import beans.User;
import dtos.UserDTO;

public class ProfileDAO {
	private Map<Long, UserDTO> users = new HashMap<Long, UserDTO>();
	private String path = null;
	
	public ProfileDAO(String context){
		this.path = context;
		loadUsers(context);
	}
	
	public Collection<UserDTO> getAll() {
		return users.values();
	}
	
	public UserDTO getById(long id) {
		return users.get(id);
	}
	
	public UserDTO findByUsername(String username) {
		for (UserDTO u: users.values()) {
			if (u.getUsername().equals(username)) {
				return u;
			}
		}
		
		return null;
	}
	
	public UserDTO findByUsernameAndPassword(String username, String password) {
		for (UserDTO u: users.values()) {
			if (u.getUsername().equals(username) && u.getPassword().equals(password)) {
				return u;
			}
		}
		
		return null;
	}
	
	public void registerANewCustomer(UserDTO newCustomer) {
		long idOfNewCustomer = generateNewId();
		users.put(idOfNewCustomer, newCustomer);
	}
	
	private long generateNewId() {
		long maxId = 0;
		for (long id: users.keySet()) {
			if (id > maxId) {
				maxId = id;
			}
		}
		
		long newId = maxId + 1;
		
		return newId;
	}
	
	public UserDTO updateUser(UserDTO userDTO) {
		UserDTO oldUser = users.get(userDTO.getId());
		
		oldUser.setFirstName(userDTO.getFirstName());
		oldUser.setDateOfBirth(userDTO.getDateOfBirth());
		oldUser.setGender(userDTO.getGender());
		oldUser.setLastName(userDTO.getLastName());
		oldUser.setPassword(userDTO.getPassword());
		oldUser.setUsername(userDTO.getUsername());
		
		SaveToFile();
		
		return oldUser;
	}
	
	private void SaveToFile() {
		BufferedWriter bw = null;
		try {	
			File file = new File(path + "/users.txt");
			System.out.println(file);
			FileOutputStream fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			for(UserDTO u : users.values()) {
				String line = 
				u.getId()+";"+u.isLogicallyDeleted()+";"+u.getUsername()+";"+u.getPassword()+
				";"+u.getFirstName()+";"+u.getLastName()+";"+u.getGender()+
				";"+u.getDateOfBirth()+";"+u.getRole();
				bw.write(line);
				bw.newLine();
			}
			bw.close();//flush
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(bw!=null) {
				try {
					bw.close();
				} catch (Exception e2) {
				}
			}
		}
	}

	
	private void loadUsers(String context) {
		BufferedReader in = null;

		try {
			File file = new File(context + "/users.txt");
			in = new BufferedReader(new FileReader(file));
			String line;
			
			String id = "";
			String logicallyDeleted = "";
			String username = "";
			String password = "";
			String firstName = "";
			String lastName = "";
			String gender = "";
			String dateOfBirth="";
			String role = "";
			
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}
				
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					id = st.nextToken().trim();
					logicallyDeleted = st.nextToken().trim();
					username = st.nextToken().trim();
					password = st.nextToken().trim();
					firstName = st.nextToken().trim();
					lastName = st.nextToken().trim();
					gender = st.nextToken().trim();
					dateOfBirth = st.nextToken().trim();
					role = st.nextToken().trim();
				}
				
				users.put(Long.parseLong(id), new UserDTO(Long.parseLong(id), 
						Boolean.parseBoolean(logicallyDeleted), username, password, 
						firstName, lastName, gender, dateOfBirth, role));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null ) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

}
	
}
