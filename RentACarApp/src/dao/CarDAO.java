package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import beans.CarKind;
import beans.CarStatus;
import beans.CarType;
import beans.FuelType;
import dtos.CarDTO;

public class CarDAO {
	private Map<Long, CarDTO> cars = new HashMap<Long, CarDTO>();
	private String path = null;
	
	public CarDAO(String context){
		this.path = context;
		loadCars(context);
	}
	
	public Collection<CarDTO> getAll(){
		return cars.values();
	}
	
	public CarDTO getCarById(long id) {
		return cars.get(id);
	}
	
	public CarDTO createNewCar(CarDTO newCar) {
		long idOfNewCar = generateNewId();
		newCar.setId(idOfNewCar);
		cars.put(idOfNewCar, newCar);
		
		Save();
		
		return newCar;
	}
	
	private long generateNewId() {
		long maxId = 0;
		for (long id: cars.keySet()) {
			if (id > maxId) {
				maxId = id;
			}
		}
		
		long newId = maxId + 1;
		
		return newId;
	}
	
	public CarDTO editCar(CarDTO newCar) {
		CarDTO car = cars.get(newCar.getId());
		car.setBrand(newCar.getBrand());
		car.setPrice(newCar.getPrice());
		car.setType(newCar.getType());
		car.setKind(newCar.getKind());
		car.setFuel(newCar.getFuel());
		car.setConsumption(newCar.getConsumption());
		car.setNoDoors(newCar.getNoDoors());
		car.setNoPeople(newCar.getNoPeople());
		car.setDescription(newCar.getDescription());
		car.setImage(newCar.getImage());
		car.setStatus(newCar.getStatus());
		
		Save();
		
		return car;
	}
	
	public CarDTO logicallyDeleteCar(long id) {
		CarDTO car = cars.get(id);
		car.setLogicallyDeleted(true);
		
		Save();
		
		return car;
	}
	
	private void loadCars(String context) {
		BufferedReader in = null;

		try {
			File file = new File(context + "/cars.txt");
			in = new BufferedReader(new FileReader(file));
			String line;
			
			String id = "";
			String logicallyDeleted = "";
			String brand = "";
			String price = "";
			String type = "";
			String rentACarId = "";
			String kind = "";
			String consumption ="";
			String noDoors = "";
			String noPeople = "";
			String description = "";
			String image = "";
			String status = "";
			String fuel = "";
			
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}
				
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					id = st.nextToken().trim();
					logicallyDeleted = st.nextToken().trim();
					brand = st.nextToken().trim();
					price = st.nextToken().trim();
					type = st.nextToken().trim();
					rentACarId = st.nextToken().trim();
					kind = st.nextToken().trim();
					fuel = st.nextToken().trim();
					consumption = st.nextToken().trim();
					noDoors = st.nextToken().trim();
					noPeople = st.nextToken().trim();
					description = st.nextToken().trim();
					image = st.nextToken().trim();
					status = st.nextToken().trim();
				}
				
				cars.put(Long.parseLong(id), new CarDTO(Long.parseLong(id), 
						Boolean.parseBoolean(logicallyDeleted), brand, 
						Double.parseDouble(price), type, Long.parseLong(rentACarId), 
						kind, fuel, Integer.parseInt(consumption), 
						Integer.parseInt(noDoors), Integer.parseInt(noPeople), 
						description, image, status));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void Save() {
		BufferedWriter bw = null;
		try {	
			File file = new File(path + "/cars.txt");
			System.out.println(file);
			
			FileOutputStream fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			for (CarDTO cDTO: cars.values()) {
				bw.write(cDTO.toString());
				bw.newLine();
			}
			
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
	}
}
