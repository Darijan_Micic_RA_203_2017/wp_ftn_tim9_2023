package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import beans.Location;

public class LocationDAO {
	private Map<Long, Location> locations = new HashMap<Long, Location>();
	private String path = null;
	
	public LocationDAO(String context){
		this.path = context;
		loadLocations(context);
	}
	
	public Collection<Location> getAll(){
		return locations.values();
	}
	
	public Location getLocationById(long id) {
		return locations.get(id);
	}
	
	public Location findByAddress(String address) {
		for (Location l: locations.values()) {
			if (l.getAddress().equals(address)) {
				return l;
			}
		}
		
		return null;
	}
	
	public Location createNewLocation(Location newLocation) {
		long idOfNewLocation = generateNewId();
		newLocation.setId(idOfNewLocation);
		locations.put(idOfNewLocation, newLocation);
		Save();
		
		return newLocation;
	}
	
	private long generateNewId() {
		long maxId = 0;
		for (long id: locations.keySet()) {
			if (id > maxId) {
				maxId = id;
			}
		}
		
		long newId = maxId + 1;
		
		return newId;
	}
	
	private void loadLocations(String context) {
		BufferedReader in = null;

		try {
			File file = new File(context + "/locations.txt");
			in = new BufferedReader(new FileReader(file));
			String line;
			
			String id = "";
			String logicallyDeleted = "";
			String longitude = "";
			String latitude = "";
			String address = "";
			
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}
				
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					id = st.nextToken().trim();
					logicallyDeleted = st.nextToken().trim();
					longitude = st.nextToken().trim();
					latitude = st.nextToken().trim();
					address = st.nextToken().trim();
				}
				
				locations.put(Long.parseLong(id), new Location(Long.parseLong(id), 
						Boolean.parseBoolean(logicallyDeleted), 
						Double.parseDouble(longitude), Double.parseDouble(latitude), 
						address));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null ) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void Save() {
		BufferedWriter bw = null;
		try {	
			File file = new File(path + "/locations.txt");
			System.out.println(file);
			
			FileOutputStream fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			for (Location l: locations.values()) {
				bw.write(l.toString());
				bw.newLine();
			}
			
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
	}
}
