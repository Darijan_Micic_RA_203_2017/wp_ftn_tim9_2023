package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import dtos.AllUsersDTO;
import dtos.UserDTO;

public class UsersDAO {
	private List<AllUsersDTO> registeredUsers = new ArrayList<AllUsersDTO>();
	private String path = null;
	
	public UsersDAO(String context){
		this.path = context;
		loadCustomers(context);
		loadAdmins(context);
		loadManagers(context);
	}
	
	public Collection<AllUsersDTO> getAll(){
		return registeredUsers;
	}
	
	private void loadCustomers(String context) {
		BufferedReader in = null;

		try {
			File file = new File(context + "/customers.txt");
			in = new BufferedReader(new FileReader(file));
			String line;
			
			String id = "";
			String logicallyDeleted = "";
			String username = "";
			String password = "";
			String firstName = "";
			String lastName = "";
			String gender = "";
			String dateOfBirth="";
			String role = "";
			String collectedPoints = "";
			String customerTypeId = "";
			
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}
				
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					id = st.nextToken().trim();
					logicallyDeleted = st.nextToken().trim();
					username = st.nextToken().trim();
					password = st.nextToken().trim();
					firstName = st.nextToken().trim();
					lastName = st.nextToken().trim();
					gender = st.nextToken().trim();
					dateOfBirth = st.nextToken().trim();
					role = st.nextToken().trim();
					collectedPoints = st.nextToken().trim();
					customerTypeId = st.nextToken().trim();
				}
				
				registeredUsers.add(new AllUsersDTO(Long.parseLong(id), Boolean.parseBoolean(logicallyDeleted), username,
						password, firstName, lastName, gender, dateOfBirth, role, Integer.parseInt(collectedPoints),
						Long.parseLong(customerTypeId), 0));
			
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null ) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void loadAdmins(String context) {
		BufferedReader in = null;

		try {
			File file = new File(context + "/admins.txt");
			in = new BufferedReader(new FileReader(file));
			String line;
			
			String id = "";
			String logicallyDeleted = "";
			String username = "";
			String password = "";
			String firstName = "";
			String lastName = "";
			String gender = "";
			String dateOfBirth="";
			String role = "";
			
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}
				
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					id = st.nextToken().trim();
					logicallyDeleted = st.nextToken().trim();
					username = st.nextToken().trim();
					password = st.nextToken().trim();
					firstName = st.nextToken().trim();
					lastName = st.nextToken().trim();
					gender = st.nextToken().trim();
					dateOfBirth = st.nextToken().trim();
					role = st.nextToken().trim();
				}
				
				registeredUsers.add(new AllUsersDTO(Long.parseLong(id), Boolean.parseBoolean(logicallyDeleted), username,
						password, firstName, lastName, gender, dateOfBirth, role, null,
						0, 0));
			
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null ) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void loadManagers(String context) {
		BufferedReader in = null;

		try {
			File file = new File(context + "/managers.txt");
			in = new BufferedReader(new FileReader(file));
			String line;
			
			String id = "";
			String logicallyDeleted = "";
			String username = "";
			String password = "";
			String firstName = "";
			String lastName = "";
			String gender = "";
			String dateOfBirth="";
			String role = "";
			String rentACarObjectId = "";
			
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}
				
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					id = st.nextToken().trim();
					logicallyDeleted = st.nextToken().trim();
					username = st.nextToken().trim();
					password = st.nextToken().trim();
					firstName = st.nextToken().trim();
					lastName = st.nextToken().trim();
					gender = st.nextToken().trim();
					dateOfBirth = st.nextToken().trim();
					role = st.nextToken().trim();
					rentACarObjectId = st.nextToken().trim();
				}
				
				registeredUsers.add(new AllUsersDTO(Long.parseLong(id), Boolean.parseBoolean(logicallyDeleted), username,
						password, firstName, lastName, gender, dateOfBirth, role, null,
						0, Long.parseLong(rentACarObjectId)));
			
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null ) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public AllUsersDTO findByUsernameAndPassword(String username, String password) {
		for (AllUsersDTO u: registeredUsers) {
			if (u.getUsername().equals(username) && u.getPassword().equals(password)) {
				return u;
			}
		}
		
		return null;
	}

	public AllUsersDTO getCustomerById(long id) {
		return registeredUsers.stream()
        .filter(user -> user.getRole().equals("CUSTOMER") && user.getId() == id)
        .findFirst()
        .orElse(null);
	}
	
	public AllUsersDTO getAdminById(long id) {
		return registeredUsers.stream()
        .filter(user -> user.getRole().equals("ADMIN") && user.getId() == id)
        .findFirst()
        .orElse(null);
	}
	
	public List<AllUsersDTO> getAllManagers() {
		List<AllUsersDTO> allManagers = new ArrayList<AllUsersDTO>();
		for (AllUsersDTO u: registeredUsers) {
			if (u.getRole().equals("MANAGER")) {
				allManagers.add(u);
			}
		}
		
		return allManagers;
	}
	
	public List<AllUsersDTO> getAllManagersWhoDoNotHaveARentingObject() {
		List<AllUsersDTO> allManagers = new ArrayList<AllUsersDTO>();
		for (AllUsersDTO u: registeredUsers) {
			if (u.getRole().equals("MANAGER") && u.getRentACarObjectId() <= 0) {
				allManagers.add(u);
			}
		}
		
		return allManagers;
	}
	
	public AllUsersDTO getManagerById(long id) {
		return registeredUsers.stream()
        .filter(user -> user.getRole().equals("MANAGER") && user.getId() == id)
        .findFirst()
        .orElse(null);
	}

	public AllUsersDTO updateUser(AllUsersDTO user) {
		AllUsersDTO oldUser = null;
		if(user.getRole().equals("CUSTOMER")) {
			oldUser = getCustomerById(user.getId());
		}else if(user.getRole().equals("MANAGER")) {
			oldUser = getManagerById(user.getId());
		}else if(user.getRole().equals("ADMIN")) {
			oldUser = getAdminById(user.getId());
		}
		oldUser.setFirstName(user.getFirstName());
		oldUser.setDateOfBirth(user.getDateOfBirth());
		oldUser.setGender(user.getGender());
		oldUser.setLastName(user.getLastName());
		oldUser.setPassword(user.getPassword());
		oldUser.setUsername(user.getUsername());
		oldUser.setCollectedPoints(user.getCollectedPoints());
		oldUser.setCustomerTypeId(user.getCustomerTypeId());
		oldUser.setRentACarObjectId(user.getRentACarObjectId());
		
		SaveToFIleCustomer();
		SaveToFIleManager();
		SaveToFIleAdmin();
		
		return oldUser;
	}
	
	private void SaveToFIleCustomer() {
		BufferedWriter bw = null;
		try {	
			File file = new File(path + "/customers.txt");
			System.out.println(file);
			FileOutputStream fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			for(AllUsersDTO u : registeredUsers) {
				if(u.getRole().equals("CUSTOMER")) {
					String line = 
							u.getId()+";"+u.isLogicallyDeleted()+";"+u.getUsername()+";"+u.getPassword()+
							";"+u.getFirstName()+";"+u.getLastName()+";"+u.getGender()+
							";"+u.getDateOfBirth()+";"+u.getRole()+";"+u.getCollectedPoints()+";"+u.getCustomerTypeId();
							bw.write(line);
							bw.newLine();
				}
				
			}
			bw.close();
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(bw!=null) {
				try {
					bw.close();
				} catch (Exception e2) {
				}
			}
		}
	}
	
	private void SaveToFIleManager() {
		BufferedWriter bw = null;
		try {	
			File file = new File(path + "/managers.txt");
			System.out.println(file);
			FileOutputStream fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			for(AllUsersDTO u : registeredUsers) {
				if(u.getRole().equals("MANAGER")) {
					String line = 
							u.getId()+";"+u.isLogicallyDeleted()+";"+u.getUsername()+";"+u.getPassword()+
							";"+u.getFirstName()+";"+u.getLastName()+";"+u.getGender()+
							";"+u.getDateOfBirth()+";"+u.getRole()+";"+u.getRentACarObjectId();
							bw.write(line);
							bw.newLine();
				}
				
			}
			bw.close();
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(bw!=null) {
				try {
					bw.close();
				} catch (Exception e2) {
				}
			}
		}
	}
	private void SaveToFIleAdmin() {
		BufferedWriter bw = null;
		try {	
			File file = new File(path + "/admins.txt");
			System.out.println(file);
			FileOutputStream fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			for(AllUsersDTO u : registeredUsers) {
				if(u.getRole().equals("ADMIN")) {
					String line = 
							u.getId()+";"+u.isLogicallyDeleted()+";"+u.getUsername()+";"+u.getPassword()+
							";"+u.getFirstName()+";"+u.getLastName()+";"+u.getGender()+
							";"+u.getDateOfBirth()+";"+u.getRole();
							bw.write(line);
							bw.newLine();
				}
				
			}
			bw.close();
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(bw!=null) {
				try {
					bw.close();
				} catch (Exception e2) {
				}
			}
		}
	}

	public AllUsersDTO addCustomer(AllUsersDTO user) {
		long maxId = 0;
		for (AllUsersDTO users: registeredUsers) {
			if (users.getId() > maxId) {
				maxId = users.getId();
			}
		}
		
		long newId = maxId + 1;
		user.setId(newId);
		registeredUsers.add(user);
		SaveToFIleCustomer();
		
		return user;
	}
	
	public AllUsersDTO createNewManager(AllUsersDTO user) {
		long maxId = 0;
		for (AllUsersDTO users: registeredUsers) {
			if (users.getId() > maxId) {
				maxId = users.getId();
			}
		}
		
		long newId = maxId + 1;
		user.setId(newId);
		registeredUsers.add(user);
		SaveToFIleManager();
		
		return user;
	}

	public AllUsersDTO findByUsername(String username) {
		for (AllUsersDTO u: registeredUsers) {
			if (u.getUsername().equals(username)) {
				return u;
			}
		}
		
		return null;
	}
}
