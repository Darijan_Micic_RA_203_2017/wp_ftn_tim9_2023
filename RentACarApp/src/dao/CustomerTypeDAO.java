package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import beans.CustomerType;

public class CustomerTypeDAO {
	private Map<Long, CustomerType> customerTypes = new HashMap<Long, CustomerType>();
	private String path = null;
	
	public CustomerTypeDAO(String context){
		this.path = context;
		loadCustomerTypes(context);
	}
	
	public Collection<CustomerType> getAll(){
		return customerTypes.values();
	}
	
	private void loadCustomerTypes(String context) {
		BufferedReader in = null;

		try {
			File file = new File(context + "/customerTypes.txt");
			in = new BufferedReader(new FileReader(file));
			String line;
			
			String id = "";
			String name = "";
			String discount = "";
			String requiredPoints = "";
			
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}
				
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					id = st.nextToken().trim();
					name = st.nextToken().trim();
					discount = st.nextToken().trim();
					requiredPoints = st.nextToken().trim();
				}
				
				
				customerTypes.put(Long.parseLong(id), new CustomerType(Long.parseLong(id),
						name, Integer.parseInt(discount), Integer.parseInt(requiredPoints)));
						
			
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null ) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public CustomerType getLocationById(long id) {
		return customerTypes.containsKey(id)? customerTypes.get(id): null;
	}

}
