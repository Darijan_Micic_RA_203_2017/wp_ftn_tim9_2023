package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.List;
import java.util.ArrayList;

import dtos.AllUsersDTO;
import dtos.CommentDTO;

public class CommentDAO {
	
	private Map<Long, CommentDTO> comments = new HashMap<Long, CommentDTO>();
	private String path = null;
	
	public CommentDAO(String context){
		this.path = context;
		loadComments(context);
	}
	
	public Collection<CommentDTO> getAll(){
		return comments.values();
	}
	
	private void loadComments(String context) {
		BufferedReader in = null;

		try {
			File file = new File(context + "/comments.txt");
			in = new BufferedReader(new FileReader(file));
			String line;
			
			String id = "";
			String logicallyDeleted = "";
			String userId = "";
			String rentACarObjectId = "";
			String comment = "";
			String rating = "";
			String status = "";
			
			
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}
				
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					id = st.nextToken().trim();
					logicallyDeleted = st.nextToken().trim();
					userId = st.nextToken().trim();
					rentACarObjectId = st.nextToken().trim();
					comment = st.nextToken().trim();
					rating = st.nextToken().trim();
					status = st.nextToken().trim();
				}
				
				comments.put(Long.parseLong(id), new CommentDTO(Long.parseLong(id), Boolean.parseBoolean(logicallyDeleted) ,Long.parseLong(userId),
						Long.parseLong(rentACarObjectId), comment, Integer.parseInt(rating), status));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null ) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public Collection<CommentDTO> getAllAcceptedFromObject(long id) {
		List<CommentDTO> returnComments = new ArrayList<CommentDTO>();
		for(CommentDTO comment: comments.values()) {
			if(comment.getRentACarObjectId() == id && comment.getStatus().equals("ACCEPTED") && comment.isLogicallyDeleted() == false) {
				returnComments.add(comment);
			}
		}
		return returnComments;
		
	}

	public Collection<CommentDTO> getAcceptedComments() {
		List<CommentDTO> returnComments = new ArrayList<CommentDTO>();
		for(CommentDTO comment: comments.values()) {
			if(comment.getStatus().equals("ACCEPTED")) {
				returnComments.add(comment);
			}
		}
		return returnComments;
	}

	public Collection<CommentDTO> getAcceptedAndRejectedComments(long id) {
		List<CommentDTO> returnComments = new ArrayList<CommentDTO>();
		for(CommentDTO comment: comments.values()) {
			if(comment.getRentACarObjectId() == id && (!comment.getStatus().equals("PENDING")) && comment.isLogicallyDeleted() == false) {
				returnComments.add(comment);
			}
		}
		return returnComments;
	}

	public CommentDTO acceptOrRejectComment(long id, String status) {
		CommentDTO oldComment = comments.get(id);
		oldComment.setStatus(status);
		
		saveComment();
		
		return oldComment;
	}
	
	private void saveComment() {
		BufferedWriter bw = null;
		try {	
			File file = new File(path + "/comments.txt");
			System.out.println(file);
			FileOutputStream fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			for(CommentDTO u : comments.values()) {
				
					String line = 
							u.getId()+";"+ u.isLogicallyDeleted() + ";"+u.getUserId()+";"+u.getRentACarObjectId()+";"+u.getComment()+
							";"+u.getRating()+";"+u.getStatus();
							bw.write(line);
							bw.newLine();
			}
			bw.close();
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(bw!=null) {
				try {
					bw.close();
				} catch (Exception e2) {
				}
			}
		}
	}

	public Collection<CommentDTO> getAllFromObject(long id) {
		List<CommentDTO> returnComments = new ArrayList<CommentDTO>();
		for(CommentDTO comment: comments.values()) {
			if(comment.getRentACarObjectId() == id && comment.isLogicallyDeleted() == false) {
				returnComments.add(comment);
			}
		}
		return returnComments;
	}

	public CommentDTO addComment(CommentDTO comment) {
		long maxId = 0;
		for (long id: comments.keySet()) {
			if (id > maxId) {
				maxId = id;
			}
		}
		
		long newId = maxId + 1;
		comment.setId(newId);
		comments.put(newId, comment);
		
		saveComment();
		
		return comment;
	}

	public CommentDTO deleteComment(long id) {
		CommentDTO oldComment = comments.get(id);
		oldComment.setLogicallyDeleted(true);
		
		saveComment();
		
		return oldComment;
	}
	
	

}
