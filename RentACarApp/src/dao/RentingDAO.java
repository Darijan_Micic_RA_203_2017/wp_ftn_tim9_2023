package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import dtos.RentingDTO;

public class RentingDAO {
	private Map<String, RentingDTO> rentings = new HashMap<String, RentingDTO>();
	private String path = null;
	
	public RentingDAO(String context){
		this.path = context;
		loadRentings(context);
	}
	
	public Collection<RentingDTO> getAll(){
		return rentings.values();
	}
	
	private void loadRentings(String context) {
		BufferedReader in = null;

		try {
			File file = new File(context + "/rentings.txt");
			in = new BufferedReader(new FileReader(file));
			String line;
			
			
			String id = "";
			String cars = "";
			String rentACarObjectId = "";
			String date = "";
			String length = "";
			String price = "";
			String customerId="";
			String status = "";
			
			
			
			
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}
				List<Long> carIds = new ArrayList<Long>();
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					id = st.nextToken().trim();
					cars = st.nextToken().trim();
					rentACarObjectId = st.nextToken().trim();
					date = st.nextToken().trim();
					length = st.nextToken().trim();
					price = st.nextToken().trim();
					customerId = st.nextToken().trim();
					status = st.nextToken().trim();
				}
				
				st = new StringTokenizer(cars, "|");
				while (st.hasMoreTokens()) {
					String carId = st.nextToken().trim();
					carIds.add(Long.parseLong(carId));
				}
				
				rentings.put(id, new RentingDTO(id, carIds, Long.parseLong(rentACarObjectId), 
						date, Integer.parseInt(length), Double.parseDouble(price),
						Long.parseLong(customerId), status));
			
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null ) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public Collection<RentingDTO> getRentingsFromObject(long id) {
		List<RentingDTO> returnDTO = new ArrayList<RentingDTO>();
		for(RentingDTO r: rentings.values()) {
			if(r.getRentACarObjectId() == id) {
				returnDTO.add(r);
			}
		}
		return returnDTO;
	}

	public Collection<RentingDTO> getRentingsFromObjectByCustomerId(long id) {
		List<RentingDTO> returnDTO = new ArrayList<RentingDTO>();
		for(RentingDTO r: rentings.values()) {
			if(r.getCustomerId() == id) {
				returnDTO.add(r);
			}
		}
		return returnDTO;
	}
}
